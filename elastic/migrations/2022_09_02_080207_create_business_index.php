<?php
declare(strict_types=1);

use App\Models\Business;
use Elastic\Adapter\Indices\Mapping;
use Elastic\Adapter\Indices\Settings;
use Elastic\Migrations\Facades\Index;
use Elastic\Migrations\MigrationInterface;

final class CreateBusinessIndex implements MigrationInterface
{
    /**
     * Run the migration.
     */
    public function up(): void
    {
        $mapping = [
            'properties' => [
                'name' => [
                    'type' => 'text',
                    'fields' => [
                        'raw' => [
                            'type' => 'keyword',
                        ]
                    ]
                ],
                'categories' => [
                    'type' => 'nested',
                ],
            ]
        ];

        Index::createIfNotExistsRaw((new Business())->searchableAs(), $mapping);
    }

    /**
     * Reverse the migration.
     */
    public function down(): void
    {
        Index::dropIfExists((new Business())->searchableAs());
    }
}
