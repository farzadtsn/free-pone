<?php

namespace App\Data\Auth;

use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;
use Spatie\LaravelData\Data;

class BusinessRegisterData extends Data
{
    public function __construct(
        public readonly string $email,
        public readonly string $password,
        public readonly string $first_name,
        public readonly string $last_name,
    ) {
    }

    public static function rules(): array
    {
        return [
            'email' => ['required', 'email', Rule::unique('users')],
            'password' => ['required', Password::min(8)->mixedCase()->symbols()->numbers()],
            'first_name' => ['required', 'min:2'],
            'last_name' => ['required', 'min:2'],
        ];
    }
}
