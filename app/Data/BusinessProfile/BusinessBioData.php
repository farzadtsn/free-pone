<?php

namespace App\Data\BusinessProfile;

use Spatie\LaravelData\Data;

class BusinessBioData extends Data
{
    public function __construct(
        public readonly string $description,
        public readonly string $owner_name,
        public readonly string $established_year,
    ) {
    }

    public static function rules(): array
    {
        $year = now()->year;

        return [
            'description' => ['required', 'string', 'min:5', 'max:1000'],
            'owner_name' => ['required', 'string', 'min:2', 'max:100'],
            'established_year' => ['required', 'integer', 'min:' . $year - 200, 'max:' . $year],
        ];
    }
}
