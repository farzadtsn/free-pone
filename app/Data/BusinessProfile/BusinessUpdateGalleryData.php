<?php

namespace App\Data\BusinessProfile;

use Spatie\LaravelData\Data;

class BusinessUpdateGalleryData extends Data
{
    public function __construct(
        public readonly string $title,
        public readonly ?int $position,
    ) {
    }

    public static function rules(): array
    {
        return [
            'title' => ['required', 'string', 'min:2', 'max:50'],
            'position' => ['integer', 'min:1', 'max:1000'],
        ];
    }
}
