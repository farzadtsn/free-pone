<?php

namespace App\Data\BusinessProfile;

use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class BusinessAddressData extends Data
{
    public function __construct(
        public readonly string $city_id,
        public readonly string $address,
        public readonly string $zip_code,
        public readonly string $latitude,
        public readonly string $longitude,
        public readonly array $service_areas,
    ) {
    }

    public static function rules(): array
    {
        $countryId = auth()->user()->business->country_id;

        return [
            'city_id' => [
                'required',
                Rule::exists('cities', 'id')->where(function ($query) use ($countryId) {
                    $query->where('country_id', $countryId);
                }),
            ],
            'address' => ['required', 'string', 'max:190'],
            'zip_code' => ['required', 'string', 'max:20'],
            'latitude' => [
                'required',
                'numeric',
                // 'regex:/^(\+|-)?(?:90(?:(?:\.0{1,8})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,8})?))$/',
            ],
            'longitude' => [
                'required',
                'numeric',
                // 'regex:/^(\+|-)?(?:180(?:(?:\.0{1,8})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,8})?))$/',
            ],
            'service_areas' => ['required', 'array', 'min:1', 'max:5'],
            'service_areas.*' => [
                'required',
                'distinct',
                Rule::exists('cities', 'id')->where(function ($query) use ($countryId) {
                    $query->where('country_id', $countryId);
                }),
            ],
        ];
    }
}
