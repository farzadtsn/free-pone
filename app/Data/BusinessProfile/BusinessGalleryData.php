<?php

namespace App\Data\BusinessProfile;

use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rules\File;
use Spatie\LaravelData\Data;

class BusinessGalleryData extends Data
{
    public function __construct(
        public readonly UploadedFile $photo,
        public readonly string $title,
        public readonly ?int $position,
    ) {
    }

    public static function rules(): array
    {
        return [
            'title' => ['required', 'string', 'min:2', 'max:50'],
            'position' => ['integer', 'min:1', 'max:1000'],
            'photo' => [
                'required',
                File::image(),
                    // ->min(40)
                    // ->max(400)
                    // ->dimensions(Rule::dimensions()->maxWidth(1000)->maxHeight(1000)),
            ],
        ];
    }
}
