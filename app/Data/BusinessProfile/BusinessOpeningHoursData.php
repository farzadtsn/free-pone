<?php

namespace App\Data\BusinessProfile;

use Spatie\LaravelData\Data;

class BusinessOpeningHoursData extends Data
{
    public function __construct(
        public readonly array $opening_hours,
    ) {
    }

    public static function rules(): array
    {
        return [
            'opening_hours' => ['required', 'array', 'size:7'],
            'opening_hours.*.day' => ['required', 'integer', 'between:0,6', 'distinct'],
            'opening_hours.*.start_hour' => [
                'required_if:opening_hours.*.is_closed,false',
                'date_format:H:i',
                'between:0,23',
            ],
            'opening_hours.*.end_hour' => [
                'required_if:opening_hours.*.is_closed,false',
                'date_format:H:i',
                'after:opening_hours.*.start_hour',
            ],
            'opening_hours.*.is_closed' => ['required', 'boolean'],
        ];
    }
}
