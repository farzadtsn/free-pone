<?php

namespace App\Data\BusinessProfile;

use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class BusinessCategoriesData extends Data
{
    public function __construct(
        public readonly array $categories,
    ) {
    }

    public static function rules(): array
    {
        return [
            'categories' => ['required', 'array', 'min:1'],
            'categories.*' => ['required', Rule::exists('categories', 'id'), 'distinct'],
        ];
    }
}
