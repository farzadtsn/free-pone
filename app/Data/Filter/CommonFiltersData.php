<?php

namespace App\Data\Filter;

use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class CommonFiltersData extends Data
{
    public function __construct(
        public readonly ?array $sort,
        public readonly int $size = 15,
        public readonly int $page = 1,
    ) {
    }

    public static function rules(): array
    {
        return [
            'sort' => ['array', 'size:2'],
            'sort.direction' => [Rule::in(['asc', 'desc'])],
            'sort.field' => ['string'],
            'size' => ['integer', 'min:2', 'max:50'],
            'page' => ['min:1', 'max:1000'],
        ];
    }
}
