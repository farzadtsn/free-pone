<?php

namespace App\Data\Filter;

use Spatie\LaravelData\Data;

class FilterData extends Data
{
    public function __construct(
        public readonly string $name,
        public readonly string $html_input_type,
        public readonly string $cast_type,
        public readonly ?array $values,
    ) {
    }
}
