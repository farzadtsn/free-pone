<?php

namespace App\Data\Comment;

use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class CommentCreateData extends Data
{
    public function __construct(
        public readonly string $body,
        public readonly string $rating,
    ) {
    }

    public static function rules(): array
    {
        return [
            'body' => ['required', 'min:5', 'max:1500'],
            'rating' => ['required', 'integer', 'min:1', 'max:5'],
            'parent_id' => ['nullable', Rule::exists('comments', 'id')],
        ];
    }
}
