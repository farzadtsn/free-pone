<?php

namespace App\Data\DiscountCampaign;

use App\Enums\DiscountCampaignLocationEnum;
use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class DiscountCampaignAppointmentData extends Data
{
    public function __construct(
        public readonly bool $is_appointment_required,
    ) {
    }

    public static function rules(): array
    {
        return [
            'is_appointment_required' => ['required', 'boolean'],
        ];
    }
}
