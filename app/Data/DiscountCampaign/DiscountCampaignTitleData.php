<?php

namespace App\Data\DiscountCampaign;

use Spatie\LaravelData\Data;

class DiscountCampaignTitleData extends Data
{
    public function __construct(
        public readonly string $name,
    ) {
    }

    public static function rules(): array
    {
        return [
            'name' => ['required', 'string', 'min:3', 'max:100'],
        ];
    }
}
