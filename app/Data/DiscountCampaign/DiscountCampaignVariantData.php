<?php

namespace App\Data\DiscountCampaign;

use Spatie\LaravelData\Data;

class DiscountCampaignVariantData extends Data
{
    public function __construct(
        public readonly string $name,
        public readonly float $price,
        public readonly int $discount_percent,
        public readonly int $quantity,
    ) {
    }

    public static function rules(): array
    {
        return [
            'name' => ['required', 'string', 'min:3', 'max:100'],
            'price' => ['required', 'numeric', 'gt:0', 'max:1000000'],
            'discount_percent' => ['required', 'integer', 'min:1', 'max:99'],
            'quantity' => ['required', 'integer', 'min:1', 'max:1000000'],
        ];
    }
}
