<?php

namespace App\Data\DiscountCampaign;

use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class DiscountCampaignCategoriesData extends Data
{
    public function __construct(
        public readonly array $categories,
    ) {
    }

    public static function rules(): array
    {
        $businessCategoriesIds = auth()->user()
            ->business
            ->categories()
            ->whereIsLeaf()
            ->pluck('categories.id');

        return [
            'categories' => ['required', 'array', 'min:1'],
            'categories.*' => ['required', Rule::in($businessCategoriesIds), 'distinct'],
        ];
    }
}
