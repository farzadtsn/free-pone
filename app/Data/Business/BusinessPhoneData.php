<?php

namespace App\Data\Business;

use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class BusinessPhoneData extends Data
{
    public function __construct(
        public readonly ?string $phone = null,
    ) {
    }

    public static function rules(): array
    {
        $countryCode = auth()->user()->business->country?->iso2;

        return [
            'phone' => [
                'nullable',
                Rule::phone()->country([$countryCode])->mobile(),
                'regex:/^[0-9]+$/',
            ],
        ];
    }
}
