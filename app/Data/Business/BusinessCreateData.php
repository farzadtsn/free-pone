<?php

namespace App\Data\Business;

use App\Models\User;

class BusinessCreateData
{
    public function __construct(
        public readonly User $user,
    ) {
    }
}
