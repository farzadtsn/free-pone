<?php

namespace App\Data\Business;

use App\Helpers\TranslateHelper;
use Carbon\Carbon;
use Spatie\LaravelData\Attributes\DataCollectionOf;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\DataCollection;

class DiscountCampaignSearchData extends Data
{
    public function __construct(
        public readonly string $name,
        public readonly string $created_at,
        public readonly string $start_date,
        public readonly string $end_date,
        public readonly int $deadline,
        public readonly array $categories,
        public BusinessSearchData $business,
        #[DataCollectionOf(DiscountCampaignVariantSearchData::class)]
        public DataCollection $variants,
        public DiscountCampaignVariantSearchData $variant,
    ) {
    }

    public function formattedResponse(): array
    {
        $categories = collect($this->categories)->map(function ($category) {
            return [
                'id' => $category['id'],
                'name' => TranslateHelper::getTranslation($category['name']),
            ];
        });

        return [
            'name' => $this->name,
            'categories' => $categories,
            'created_at' => Carbon::parse($this->created_at)->format('Y-m-d'),
            'start_date' => Carbon::parse($this->start_date)->format('Y-m-d'),
            'end_date' => Carbon::parse($this->end_date)->format('Y-m-d'),
            'deadline' => $this->deadline,
            'business' => $this->business->formattedResponse(),
            'variants' => $this->variants,
            'variant' => $this->variant,
        ];
    }
}
