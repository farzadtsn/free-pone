<?php

namespace App\Data\Business;

use App\Helpers\TranslateHelper;
use App\Models\Business;
use Carbon\Carbon;
use Spatie\LaravelData\Data;

class BusinessSearchData extends Data
{
    public function __construct(
        public readonly string $name,
        public readonly string $address,
        public readonly string $created_at,
        public readonly ?int $established_year,
        public readonly array $categories,
        public readonly array $country,
        public readonly array $city,
        public readonly array $ratings,
        public readonly ?array $logo,
    ) {
    }

    public function formattedResponse(): array
    {
        $countryName = TranslateHelper::getTranslation($this->country['name']);

        $categories = collect($this->categories)->map(function ($category) {
            return [
                'id' => $category['id'],
                'name' => TranslateHelper::getTranslation($category['name']),
            ];
        });

        return [
            'name' => $this->name,
            'address' => $this->address . ', ' . ($this->city['name'] ?? '') . ', ' . $countryName,
            'country' => [
                'name' => $countryName,
                'id' => $this->country['id'],
            ],
            'city' => [
                'name' => $this->city['name'],
                'id' => $this->city['id'],
            ],
            'categories' => $categories,
            'established_year' => $this->established_year,
            'registered_at' => Carbon::parse($this->created_at)->format('Y-m-d'),
            'ratings' => $this->ratings,
            'logo' => $this->logo ?: Business::defaultLogo(),
        ];
    }
}
