<?php

namespace App\Data\Business;

use Spatie\LaravelData\Data;

class LocationData extends Data
{
    public function __construct(
        public readonly float $lat,
        public readonly float $long,
        public readonly float $distance,
    ) {
    }

    public static function rules(): array
    {
        return [
            'lat' => ['required_with:location', 'numeric'],
            'long' => ['required_with:location', 'numeric'],
            'distance' => ['required_with:location', 'numeric'],
        ];
    }
}
