<?php

namespace App\Data\Business;

use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class BusinessAddressData extends Data
{
    public function __construct(
        public readonly string $city_id,
        public readonly string $address,
        public readonly string $zip_code,
    ) {
    }

    public static function rules(): array
    {
        return [
            'city_id' => ['required', Rule::exists('cities', 'id')],
            'address' => ['required', 'string', 'max:190'],
            'zip_code' => ['required', 'string', 'max:20'],
        ];
    }
}
