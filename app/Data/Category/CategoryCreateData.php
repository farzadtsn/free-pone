<?php

namespace App\Data\Category;

use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class CategoryCreateData extends Data
{
    public function __construct(
        public readonly string $name,
        public readonly ?int $parent_id = null,
    ) {
    }

    public static function rules(): array
    {
        return [
            'name' => ['required', 'min:2'],
            'parent_id' => ['nullable', Rule::exists('categories', 'id')],
        ];
    }
}
