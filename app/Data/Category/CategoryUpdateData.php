<?php

namespace App\Data\Category;

use Illuminate\Validation\Rule;
use Spatie\LaravelData\Data;

class CategoryUpdateData extends Data
{
    public function __construct(
        public readonly string $name,
        public readonly string $locale = 'en',
    ) {
    }

    public static function rules(): array
    {
        return [
            'name' => ['required', 'min:2'],
            'locale' => ['nullable', Rule::in(['en', 'fa'])],
        ];
    }
}
