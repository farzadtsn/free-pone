<?php

namespace App\Http\Controllers\Client;

use App\Actions\Search\DiscountCampaign\DiscountCampaignSearchAction;
use App\Data\Business\BusinessFilterData;
use App\Data\Filter\CommonFiltersData;
use App\Http\Controllers\Controller;
use App\Http\Resources\Client\DiscountCampaign\SearchResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class SearchController extends Controller
{
    public function suggestions(
        BusinessFilterData $filters,
        CommonFiltersData $commonFilters,
        DiscountCampaignSearchAction $discountCampaignSearchAction,
    ) {
        return Response::success(
            message: '',
            data:  SearchResource::collection(
                $discountCampaignSearchAction->execute($filters, $commonFilters, false)->hits()
            ),
        );
    }

    public function searchDiscountCampaign(
        BusinessFilterData $filters,
        CommonFiltersData $commonFilters,
        DiscountCampaignSearchAction $discountCampaignSearchAction,
    ) :JsonResponse {
        return Response::success(
            message: '',
            data:  SearchResource::collection(
                $discountCampaignSearchAction->execute($filters, $commonFilters, true)
            )
            ->response()
            ->getData(true),
        );
    }
}
