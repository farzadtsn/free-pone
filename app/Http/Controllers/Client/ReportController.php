<?php

namespace App\Http\Controllers\Client;

use App\Actions\Attachment\AttachmentReportAction;
use App\Actions\Business\BusinessReportAction;
use App\Data\Report\ReportData;
use App\Http\Controllers\Controller;
use App\Http\Resources\Client\Report\ReportResource;
use App\Models\Attachment;
use App\Models\Business;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class ReportController extends Controller
{
    public function reportBusiness(
        ReportData $data,
        BusinessReportAction $businessReportAction,
        Business $business,
    ) :JsonResponse {
        $report = $businessReportAction->execute($data, $business);

        return Response::success(
            message: 'reported successfully',
            data: ReportResource::make($report),
        );
    }

    public function reportAttachment(
        ReportData $data,
        AttachmentReportAction $attachmentReportAction,
        Attachment $attachment,
    ) :JsonResponse {
        $report = $attachmentReportAction->execute($data, $attachment);

        return Response::success(
            message: 'reported successfully',
            data: ReportResource::make($report),
        );
    }
}
