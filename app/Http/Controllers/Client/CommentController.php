<?php

namespace App\Http\Controllers\Client;

use App\Actions\Comment\CommentCreateAction;
use App\Data\Comment\CommentCreateData;
use App\Data\Filter\CommonFiltersData;
use App\Http\Controllers\Controller;
use App\Http\Resources\Client\Comment\CommentCollection;
use App\Models\Business;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class CommentController extends Controller
{
    public function index(
        CommonFiltersData $commonFiltersData,
        Business $business,
    ) :JsonResponse {
        $comments = $business
            ->comments()
            ->with('user')
            ->withStatistics()
            ->sort($commonFiltersData->sort)
            ->paginate($commonFiltersData->size);

        return Response::success(
            message: '',
            data:  CommentCollection::make($comments)
                ->response()
                ->getData(true),
        );
    }

    public function create(
        CommentCreateData $commentCreateData,
        CommentCreateAction $commentCreateAction,
        Business $business,
    ) :JsonResponse {
        $commentCreateAction->execute($commentCreateData, $business, auth()->user());

        return Response::success(
            message: trans('messages.comment_added'),
            data: [],
        );
    }
}
