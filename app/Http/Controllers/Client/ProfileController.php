<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Resources\Client\Transaction\TransactionResource;
use Illuminate\Support\Facades\Response;

class ProfileController extends Controller
{
    public function transactions()
    {
        $transactions = auth()->user()
            ->transactions()
            ->with('paymentGateway')
            ->latest('id')
            ->paginate();

        return Response::success(
            message: '',
            data: TransactionResource::collection($transactions)
                    ->response()
                    ->getData(true),
        );
    }
}
