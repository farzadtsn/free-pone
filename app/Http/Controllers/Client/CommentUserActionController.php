<?php

namespace App\Http\Controllers\Client;

use App\Actions\UserAction\UserActionAddAction;
use App\Actions\UserAction\UserActionRemoveAction;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class CommentUserActionController extends Controller
{
    public function addAction(
        UserActionAddAction $userActionAddAction,
        Comment $comment,
        string $action,
    ) :JsonResponse {
        $userActionAddAction->execute($comment, auth()->user(), $action);

        return Response::success(
            message: trans('messages.action_added'),
            data: [],
        );
    }

    public function removeAction(
        UserActionRemoveAction $userActionRemoveAction,
        Comment $comment,
        string $action,
    ) :JsonResponse {
        $userActionRemoveAction->execute($comment, auth()->user(), $action);

        return Response::success(
            message: trans('messages.action_removed'),
            data: [],
        );
    }
}
