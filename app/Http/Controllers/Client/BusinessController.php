<?php

namespace App\Http\Controllers\Client;

use App\Actions\Business\BusinessLoadRelationAction;
use App\Http\Controllers\Controller;
use App\Http\Resources\Client\Business\BusinessResource;
use App\Models\Business;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class BusinessController extends Controller
{
    public function show(
        BusinessLoadRelationAction $businessLoadRelationAction,
        Business $business,
    ) :JsonResponse {
        $businessLoadRelationAction->execute($business);

        return Response::success(
            message: trans('messages.action_added'),
            data: BusinessResource::make($business),
        );
    }
}
