<?php

namespace App\Http\Controllers\Business;

use App\Actions\Business\BusinessSetAddressAction;
use App\Actions\Business\BusinessSetInfoAction;
use App\Actions\Business\BusinessSetPhoneAction;
use App\Data\Business\BusinessAddressData;
use App\Data\Business\BusinessInfoData;
use App\Data\Business\BusinessPhoneData;
use App\Http\Controllers\Controller;
use App\Http\Resources\Business\Business\UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class BusinessRegisterStepsController extends Controller
{
    public function setInfo(
        BusinessInfoData $data,
        BusinessSetInfoAction $businessSetInfoAction,
    ) :JsonResponse {
        $user = $businessSetInfoAction->execute($data, auth()->user());

        return Response::success(
            message: '',
            data:  UserResource::make($user->load('business.categories', 'business.country', 'business.city', 'business.state')),
        );
    }

    public function setPhone(
        BusinessPhoneData $data,
        BusinessSetPhoneAction $businessSetPhoneAction,
    ) :JsonResponse {
        $user = $businessSetPhoneAction->execute($data, auth()->user());

        return Response::success(
            message: '',
            data:  UserResource::make($user->load('business.categories', 'business.country', 'business.city', 'business.state')),
        );
    }

    public function setAddress(
        BusinessAddressData $data,
        BusinessSetAddressAction $businessSetAddressAction,
    ) :JsonResponse {
        $user = $businessSetAddressAction->execute($data, auth()->user());

        return Response::success(
            message: '',
            data:  UserResource::make($user->load('business.categories', 'business.country', 'business.city', 'business.state')),
        );
    }
}
