<?php

namespace App\Http\Controllers\Business;

use App\Actions\DiscountCampaign\DiscountCampaignAddPhotoAction;
use App\Actions\DiscountCampaign\DiscountCampaignAddVariantAction;
use App\Actions\DiscountCampaign\DiscountCampaignSetAppointmentRequiredAction;
use App\Actions\DiscountCampaign\DiscountCampaignSetCategoriesAction;
use App\Actions\DiscountCampaign\DiscountCampaignSetDatesAction;
use App\Actions\DiscountCampaign\DiscountCampaignSetDescriptionAction;
use App\Actions\DiscountCampaign\DiscountCampaignSetLocationAction;
use App\Actions\DiscountCampaign\DiscountCampaignSetTitleAction;
use App\Data\DiscountCampaign\DiscountCampaignAppointmentData;
use App\Data\DiscountCampaign\DiscountCampaignCategoriesData;
use App\Data\DiscountCampaign\DiscountCampaignDateData;
use App\Data\DiscountCampaign\DiscountCampaignDescriptionData;
use App\Data\DiscountCampaign\DiscountCampaignLocationData;
use App\Data\DiscountCampaign\DiscountCampaignPhotoData;
use App\Data\DiscountCampaign\DiscountCampaignTitleData;
use App\Data\DiscountCampaign\DiscountCampaignVariantData;
use App\Http\Controllers\Controller;
use App\Http\Resources\Client\DiscountCampaign\DiscountCampaignResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class BusinessDiscountCampaignStepsController extends Controller
{
    public function setCategories(
        DiscountCampaignCategoriesData $data,
        DiscountCampaignSetCategoriesAction $discountCampaignSetCategoriesAction
    ): JsonResponse {
        $discountCampaign = $discountCampaignSetCategoriesAction->execute($data, auth()->user()->business);

        return Response::success(
            message: '',
            data:  DiscountCampaignResource::make($discountCampaign->load('categories')),
        );
    }

    public function setTitle(
        DiscountCampaignTitleData $data,
        DiscountCampaignSetTitleAction $action
    ): JsonResponse {
        $discountCampaign = $action->execute($data, auth()->user()->business);

        return Response::success(
            message: '',
            data:  DiscountCampaignResource::make($discountCampaign->load('categories')),
        );
    }

    public function setDescription(
        DiscountCampaignDescriptionData $data,
        DiscountCampaignSetDescriptionAction $action
    ): JsonResponse {
        $discountCampaign = $action->execute($data, auth()->user()->business);

        return Response::success(
            message: '',
            data:  DiscountCampaignResource::make($discountCampaign->load('categories')),
        );
    }

    public function addPhoto(
        DiscountCampaignPhotoData $data,
        DiscountCampaignAddPhotoAction $action
    ): JsonResponse {
        $discountCampaign = $action->execute($data, auth()->user()->business);

        return Response::success(
            message: '',
            data:  DiscountCampaignResource::make($discountCampaign->load('categories', 'photos', 'variants')),
        );
    }

    public function addVariant(
        DiscountCampaignVariantData $data,
        DiscountCampaignAddVariantAction $action
    ): JsonResponse {
        $discountCampaign = $action->execute($data, auth()->user()->business);

        return Response::success(
            message: '',
            data:  DiscountCampaignResource::make($discountCampaign->load('categories', 'photos', 'variants')),
        );
    }

    public function setLocation(
        DiscountCampaignLocationData $data,
        DiscountCampaignSetLocationAction $action
    ): JsonResponse {
        $discountCampaign = $action->execute($data, auth()->user()->business);

        return Response::success(
            message: '',
            data:  DiscountCampaignResource::make($discountCampaign->load('categories', 'photos', 'variants')),
        );
    }

    public function setAppointmentType(
        DiscountCampaignAppointmentData $data,
        DiscountCampaignSetAppointmentRequiredAction $action
    ): JsonResponse {
        $discountCampaign = $action->execute($data, auth()->user()->business);

        return Response::success(
            message: '',
            data:  DiscountCampaignResource::make($discountCampaign->load('categories', 'photos', 'variants')),
        );
    }

    public function setDates(
        DiscountCampaignDateData $data,
        DiscountCampaignSetDatesAction $action
    ): JsonResponse {
        $discountCampaign = $action->execute($data, auth()->user()->business);

        return Response::success(
            message: '',
            data:  DiscountCampaignResource::make($discountCampaign->load('categories', 'photos', 'variants')),
        );
    }
}
