<?php

namespace App\Http\Controllers\Admin;

use App\Actions\Attachment\AttachmentApproveAction;
use App\Actions\Attachment\AttachmentRejectAction;
use App\Data\Attachment\AttachmentFilterData;
use App\Data\Attachment\AttachmentRejectData;
use App\Data\Filter\CommonFiltersData;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Attachment\AttachmentResource;
use App\Models\Attachment;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class AttachmentController extends Controller
{
    public function index(
        AttachmentFilterData $filterData,
        CommonFiltersData $commonFiltersData,
    ): JsonResponse {
        $attachments = Attachment::query()
            ->withCount('reports')
            ->filter($filterData)
            ->sort($commonFiltersData->sort)
            ->paginate($commonFiltersData->size);

        return Response::success(
            message: '',
            data: AttachmentResource::collection($attachments)
                ->response()
                ->getData(true),
        );
    }

    public function approve(
        AttachmentApproveAction $attachmentApproveAction,
        Attachment $attachment,
    ):JsonResponse {
        return Response::success(
            message: trans('messages.approved_successfully'),
            data: AttachmentResource::make(
                $attachmentApproveAction->execute($attachment)
            ),
        );
    }

    public function reject(
        AttachmentRejectData $attachmentRejectData,
        AttachmentRejectAction $attachmentRejectAction,
        Attachment $attachment,
    ):JsonResponse {
        return Response::success(
            message: trans('messages.rejected_successfully'),
            data: AttachmentResource::make(
                $attachmentRejectAction->execute($attachmentRejectData, $attachment),
            ),
        );
    }

    public function filters():JsonResponse
    {
        return Response::success(
            message: '',
            data: AttachmentFilterData::getSelectableFilters(),
        );
    }
}
