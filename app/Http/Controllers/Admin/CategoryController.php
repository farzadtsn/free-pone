<?php

namespace App\Http\Controllers\Admin;

use App\Actions\Category\CategoryCreateAction;
use App\Actions\Category\CategoryDeleteAction;
use App\Actions\Category\CategoryUpdateAction;
use App\Data\Category\CategoryCreateData;
use App\Data\Category\CategoryUpdateData;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Category\CategoryResource;
use App\Models\Category;
use Illuminate\Support\Facades\Response;

class CategoryController extends Controller
{
    public function index()
    {
        return Response::success(
            message: '',
            data: CategoryResource::collection(
                Category::latest('id')->paginate()
            )
            ->response()
            ->getData(true),
        );
    }

    public function store(CategoryCreateData $categoryCreateData, CategoryCreateAction $categoryCreateAction)
    {
        return Response::success(
            message: trans('messages.created_successfully'),
            data: $categoryCreateAction->execute($categoryCreateData),
        );
    }

    public function show(Category $category)
    {
        return Response::success(
            message: '',
            data: $category->load('children'),
        );
    }

    public function update(
        CategoryUpdateData $categoryUpdateData,
        Category $category,
        CategoryUpdateAction $categoryUpdateAction,
    ) {
        return Response::success(
            message: trans('messages.updated_successfully'),
            data: $categoryUpdateAction->execute($categoryUpdateData, $category),
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category, CategoryDeleteAction $categoryDeleteAction)
    {
        $categoryDeleteAction->execute($category);

        return Response::success(
            message: trans('messages.deleted_successfully'),
        );
    }
}
