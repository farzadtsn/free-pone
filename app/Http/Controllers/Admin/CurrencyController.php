<?php

namespace App\Http\Controllers\Admin;

use App\Actions\Currency\CurrencyCreateAction;
use App\Actions\Currency\CurrencyDeleteAction;
use App\Actions\Currency\CurrencyUpdateAction;
use App\Data\Currency\CurrencyCreateData;
use App\Data\Currency\CurrencyUpdateData;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Currency\CurrencyResource;
use App\Models\Currency;
use Illuminate\Support\Facades\Response;

class CurrencyController extends Controller
{
    public function index()
    {
        return Response::success(
            message: '',
            data: CurrencyResource::collection(
                Currency::latest('id')->paginate()
            )
            ->response()
            ->getData(true),
        );
    }

    public function store(CurrencyCreateData $currencyCreateData, CurrencyCreateAction $currencyCreateAction)
    {
        return Response::success(
            message: trans('messages.created_successfully'),
            data: $currencyCreateAction->execute($currencyCreateData),
        );
    }

    public function show(Currency $currency)
    {
        return Response::success(
            message: '',
            data: $currency->load('paymentGateways'),
        );
    }

    public function update(
        CurrencyUpdateData $currencyUpdateData,
        Currency $currency,
        CurrencyUpdateAction $currencyUpdateAction,
    ) {
        return Response::success(
            message: trans('messages.updated_successfully'),
            data: $currencyUpdateAction->execute($currencyUpdateData, $currency),
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currency $currency, CurrencyDeleteAction $currencyDeleteAction)
    {
        $currencyDeleteAction->execute($currency);

        return Response::success(
            message: trans('messages.deleted_successfully'),
        );
    }
}
