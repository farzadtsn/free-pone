<?php

namespace App\Http\Controllers\BaseInfo;

use App\Http\Controllers\Controller;
use App\Http\Resources\Client\Currency\CurrencyResource;
use App\Models\Currency;
use Illuminate\Support\Facades\Response;

class CurrencyController extends Controller
{
    public function index()
    {
        return Response::success(
            message: '',
            data: CurrencyResource::collection(
                Currency::all()
            ),
        );
    }
}
