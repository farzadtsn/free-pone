<?php

namespace App\Http\Controllers\BaseInfo;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseInfo\Country\CityResource;
use App\Models\State;
use Illuminate\Support\Facades\Response;

class StateController extends Controller
{
    public function cities(State $state)
    {
        return Response::success(
            message: '',
            data: CityResource::collection($state->cities),
        );
    }
}
