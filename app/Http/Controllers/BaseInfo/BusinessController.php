<?php

namespace App\Http\Controllers\BaseInfo;

use App\Enums\BusinessTypeEnum;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class BusinessController extends Controller
{
    public function businessTypes()
    {
        return Response::success(
            message: '',
            data: BusinessTypeEnum::labels(),
        );
    }
}
