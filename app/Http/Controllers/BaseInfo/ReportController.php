<?php

namespace App\Http\Controllers\BaseInfo;

use App\Enums\PhotoReportEnum;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class ReportController extends Controller
{
    public function photoReportsType()
    {
        return Response::success(
            message: '',
            data: PhotoReportEnum::labels(),
        );
    }
}
