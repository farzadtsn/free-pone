<?php

namespace App\Http\Controllers\BaseInfo;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseInfo\FacilityGroup\FacilityGroupResource;
use App\Models\FacilityGroup;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class FacilityController extends Controller
{
    public function groups(): JsonResponse
    {
        return Response::success(
            message: '',
            data:  FacilityGroupResource::collection(
                FacilityGroup::orderBy('position', 'asc')->with('facilities')->get(),
            ),
        );
    }
}
