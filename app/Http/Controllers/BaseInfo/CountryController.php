<?php

namespace App\Http\Controllers\BaseInfo;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseInfo\Country\CityResource;
use App\Http\Resources\BaseInfo\Country\CountryResource;
use App\Http\Resources\BaseInfo\Country\StateResource;
use App\Models\Country;
use Illuminate\Support\Facades\Response;

class CountryController extends Controller
{
    public function show(Country $country)
    {
        return Response::success(
            message: '',
            data: CountryResource::make($country),
        );
    }

    public function index()
    {
        $countries = Country::get(['id', 'name', 'phone_code', 'iso2']);

        return Response::success(
            message: '',
            data: CountryResource::collection($countries),
        );
    }

    public function states(Country $country)
    {
        return Response::success(
            message: '',
            data: StateResource::collection($country->states),
        );
    }

    public function cities(Country $country)
    {
        return Response::success(
            message: '',
            data: CityResource::collection($country->cities),
        );
    }
}
