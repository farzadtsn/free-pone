<?php

namespace App\Http\Controllers\Auth;

use App\Actions\Auth\LoginAction;
use App\Data\Auth\LoginData;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class LoginController extends Controller
{
    public function login(LoginData $loginData, LoginAction $loginAction)
    {
        return Response::success(
            message: trans('messages.logged_in_successfully'),
            data: $loginAction->execute($loginData)
        );
    }
}
