<?php

namespace App\Http\Controllers\Auth;

use App\Actions\Auth\BusinessRegisterAction;
use App\Data\Auth\BusinessRegisterData;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class BusinessRegisterController extends Controller
{
    public function register(BusinessRegisterData $data, BusinessRegisterAction $action)
    {
        return Response::success(
            message: trans('messages.registered_successfully'),
            data: $action->execute($data)
        );
    }
}
