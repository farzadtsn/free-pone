<?php

namespace App\Http\Controllers\Auth;

use App\Actions\Auth\RegisterAction;
use App\Data\Auth\RegisterData;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class RegisterController extends Controller
{
    public function register(RegisterData $registerData, RegisterAction $registerAction)
    {
        return Response::success(
            message: trans('messages.register_successfully'),
            data: $registerAction->execute($registerData)
        );
    }
}
