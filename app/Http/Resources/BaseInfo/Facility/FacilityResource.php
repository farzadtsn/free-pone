<?php

namespace App\Http\Resources\BaseInfo\Facility;

use App\Http\Resources\BaseInfo\FacilityGroup\FacilityGroupResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FacilityResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'position' => $this->position,
            'icon' => $this->icon,
            'facility_group' => FacilityGroupResource::make($this->whenLoaded('facilityGroup')),
        ];
    }
}
