<?php

namespace App\Http\Resources\BaseInfo\Country;

use Illuminate\Http\Resources\Json\JsonResource;

class CountryResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone_code' => $this->phone_code,
            'iso_code' => $this->iso2,
            'flag' => $this->flag,
        ];
    }
}
