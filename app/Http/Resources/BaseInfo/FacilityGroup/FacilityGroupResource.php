<?php

namespace App\Http\Resources\BaseInfo\FacilityGroup;

use App\Http\Resources\BaseInfo\Facility\FacilityResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FacilityGroupResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'position' => $this->position,
            'icon' => $this->icon,
            'facilities' => FacilityResource::collection($this->whenLoaded('facilities')),
        ];
    }
}
