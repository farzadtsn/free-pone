<?php

namespace App\Http\Resources\Client\DiscountCampaign;

use App\Http\Resources\BaseInfo\Category\CategoryResource;
use App\Http\Resources\Client\Attachment\AttachmentResource;
use App\Http\Resources\Client\DiscountCampaignVariant\DiscountCampaignVariantResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DiscountCampaignResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'location_enum' => $this->location_enum,
            'is_appointment_required' => $this->is_appointment_required,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'deadline' => $this->deadline,
            'state_enum' => $this->state_enum,
            'categories' => CategoryResource::collection($this->whenLoaded('categories')),
            'photos' => AttachmentResource::collection($this->whenLoaded('photos')),
            'variants' => DiscountCampaignVariantResource::collection($this->whenLoaded('variants')),
        ];
    }
}
