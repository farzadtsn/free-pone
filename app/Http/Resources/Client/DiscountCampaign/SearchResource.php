<?php

namespace App\Http\Resources\Client\DiscountCampaign;

use App\Data\Business\DiscountCampaignSearchData;
use Illuminate\Http\Resources\Json\JsonResource;

class SearchResource extends JsonResource
{
    public function toArray($request)
    {
        $variant = $this->innerHits()->get('variants')->first()->document()->content();

        return DiscountCampaignSearchData::from(
            array_merge($this->document()->content(), ['variant' => $variant])
        )->formattedResponse();
    }
}
