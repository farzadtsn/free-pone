<?php

namespace App\Http\Resources\Client\DiscountCampaignVariant;

use Illuminate\Http\Resources\Json\JsonResource;

class DiscountCampaignVariantResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'discount_percent' => $this->discount_percent,
            'discounted_price' => $this->discounted_price,
            'quantity' => $this->quantity,
            'remained_quantity' => $this->remained_quantity,
        ];
    }
}
