<?php

namespace App\Http\Resources\Client\Media;

use Illuminate\Http\Resources\Json\JsonResource;

class MediaResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'file_name' => $this->file_name,
            'url' => $this->original_url,
            'thumbnail_url' => $this->getUrl('thumb'),
        ];
    }
}
