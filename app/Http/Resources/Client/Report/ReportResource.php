<?php

namespace App\Http\Resources\Client\Report;

use Illuminate\Http\Resources\Json\JsonResource;

class ReportResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'subject' => $this->subject,
            'description' => $this->description,
            'reportable' => $this->whenLoaded('reportable'),
        ];
    }
}
