<?php

namespace App\Http\Resources\Client\PaymentGateway;

use App\Http\Resources\Client\Currency\CurrencyResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PaymentGatewayResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'data' => $this->data,
            'logo' => $this->logo,
            'currencies' => CurrencyResource::collection(
                $this->whenLoaded('currencies')
            ),
        ];
    }
}
