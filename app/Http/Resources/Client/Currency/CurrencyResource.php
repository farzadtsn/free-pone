<?php

namespace App\Http\Resources\Client\Currency;

use Illuminate\Http\Resources\Json\JsonResource;

class CurrencyResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'symbol' => $this->symbol,
            'iso_code' => $this->iso_code,
            'exchange_rate' => $this->exchange_rate,
            'logo' => $this->logo,
        ];
    }
}
