<?php

namespace App\Http\Resources\Business\Business;

use App\Http\Resources\BaseInfo\Category\CategoryResource;
use App\Http\Resources\BaseInfo\Country\CityResource;
use App\Http\Resources\BaseInfo\Country\CountryResource;
use App\Http\Resources\BaseInfo\Country\StateResource;
use App\Http\Resources\BaseInfo\Facility\FacilityResource;
use App\Http\Resources\Client\Attachment\AttachmentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BusinessResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'website' => $this->website,
            'phone' => $this->phone,
            'step' => $this->step,
            'is_closed' => $this->is_closed,
            'reopen_date' => $this->reopen_date,
            'closed_reason' => $this->closed_reason,
            'established_year' => $this->established_year,
            'description' => $this->description,
            'owner_name' => $this->owner_name,
            'statistics' => $this->getStatistics(),
            'opening_hours' => $this->opening_hours,
            'categories' => CategoryResource::collection($this->whenLoaded('categories')),
            'facilities' => FacilityResource::collection($this->whenLoaded('facilities')),
            'country' => CountryResource::make($this->whenLoaded('country')),
            'state' => StateResource::make($this->whenLoaded('state')),
            'city' => CityResource::make($this->whenLoaded('city')),
            'service_areas' => CityResource::collection($this->whenLoaded('serviceAreas')),
            'gallery' => AttachmentResource::collection($this->whenLoaded('gallery')),
            'logo' => AttachmentResource::make($this->whenLoaded('logo')),
        ];
    }
}
