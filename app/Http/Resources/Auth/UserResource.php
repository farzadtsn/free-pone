<?php

namespace App\Http\Resources\Auth;

use App\Http\Resources\Client\Business\BusinessResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role,
            'email_verified_at' => $this->email_verified_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'business' => BusinessResource::make($this->whenLoaded('business')),
        ];
    }
}
