<?php

namespace App\Providers;

use App\Models\Attachment;
use App\Models\Business;
use App\Models\Comment;
use App\Models\DiscountCampaign;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class MorphMapServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::enforceMorphMap([
            'businesses' => Business::class,
            'attachments' => Attachment::class,
            'comments' => Comment::class,
            'discount_campaigns' => DiscountCampaign::class,
        ]);
    }
}
