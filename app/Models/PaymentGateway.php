<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentGateway extends Model
{
    use HasFactory;

    protected $casts = [
        'data'  => 'array',
    ];

    public function currencies()
    {
        return $this->belongsToMany(Currency::class);
    }
}
