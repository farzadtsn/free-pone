<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DiscountCampaignVariant extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function discountCampaign()
    {
        return $this->belongsTo(DiscountCampaign::class);
    }

    public function getDiscountedPriceAttribute()
    {
        return $this->price - $this->price * $this->discount_percent / 100;
    }

    public function getInStockAttribute()
    {
        return $this->remained_quantity > 0;
    }

    public function toSearchableArray()
    {
        return [
            'name' => $this->name,
            'quantity' => $this->quantity,
            'remained_quantity' => $this->remained_quantity,
            'in_stock' => $this->in_stock,
            'price' => $this->price,
            'discount_percent' => $this->discount_percent,
            'discounted_price' => $this->discounted_price,
        ];
    }
}
