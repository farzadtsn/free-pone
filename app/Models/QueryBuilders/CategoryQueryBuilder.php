<?php

namespace App\Models\QueryBuilders;

use Kalnoy\Nestedset\QueryBuilder;

class CategoryQueryBuilder extends QueryBuilder
{
    public function leafWithRoot(): self
    {
        return $this->whereIsLeaf()->with(['ancestors' => function ($query) {
            $query->whereIsRoot();
        }]);
    }
}
