<?php

namespace App\Models;

use App\Enums\UserActionTypeEnum;
use App\Models\QueryBuilders\CommentQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function newEloquentBuilder($query): CommentQueryBuilder
    {
        return new CommentQueryBuilder($query);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function commentable()
    {
        return $this->morphTo();
    }

    public function userActions()
    {
        return $this->morphMany(UserAction::class, 'actionable');
    }

    public function likes()
    {
        return $this->morphMany(UserAction::class, 'actionable')
            ->where('type_enum', UserActionTypeEnum::LIKE);
    }

    public function dislikes()
    {
        return $this->morphMany(UserAction::class, 'actionable')
            ->where('type_enum', UserActionTypeEnum::DISLIKE);
    }
}
