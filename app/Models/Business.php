<?php

namespace App\Models;

use App\Actions\Business\BusinessStatisticsAction;
use App\Actions\Comment\CommentsRatingsStatisticsCalculateAction;
use App\Enums\AttachmentTypeEnum;
use App\Enums\DiscountCampaignStateEnum;
use App\Enums\UserActionTypeEnum;
use App\Http\Resources\Client\Media\MediaResource;
use App\Models\QueryBuilders\BusinessQueryBuilder;
use Elastic\ScoutDriverPlus\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    use HasFactory;
    use Searchable;

    protected $guarded = [];

    public function newEloquentBuilder($query): BusinessQueryBuilder
    {
        return new BusinessQueryBuilder($query);
    }

    protected $casts = [
        'opening_hours' => 'json',
        'reopen_date' => 'date',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function discountCampaigns()
    {
        return $this->hasMany(DiscountCampaign::class);
    }

    public function facilities()
    {
        return $this->belongsToMany(Facility::class)
            ->orderBy('position', 'asc');
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function serviceAreas()
    {
        return $this->belongsToMany(City::class);
    }

    public function gallery()
    {
        return $this->morphMany(Attachment::class, 'model')
            ->where('type_enum', AttachmentTypeEnum::BUSINESS_GALLERY)
            ->orderBy('position', 'asc');
    }

    public function logo()
    {
        return $this->morphOne(Attachment::class, 'model')
            ->where('type_enum', AttachmentTypeEnum::BUSINESS_LOGO);
    }

    public function reports()
    {
        return $this->morphMany(Report::class, 'reportable');
    }

    public function userActions()
    {
        return $this->morphMany(UserAction::class, 'actionable');
    }

    public function likes()
    {
        return $this->morphMany(UserAction::class, 'actionable')
            ->where('type_enum', UserActionTypeEnum::LIKE);
    }

    public function dislikes()
    {
        return $this->morphMany(UserAction::class, 'actionable')
            ->where('type_enum', UserActionTypeEnum::DISLIKE);
    }

    public function suggests()
    {
        return $this->morphMany(UserAction::class, 'actionable')
            ->where('type_enum', UserActionTypeEnum::SUGGEST);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function getUserActions(User $user = null)
    {
        $user = $user ?: auth()->user();
        if (is_null($user)) {
            return [
                'liked' => false,
                'disliked' => false,
                'suggested' => false,
            ];
        }

        return [
            'liked' => (bool) $this
                ->userActions
                ->where('user_id', $user->id)
                ->where('type_enum', UserActionTypeEnum::LIKE)
                ->count(),
            'disliked' => (bool) $this
                ->userActions
                ->where('user_id', $user->id)
                ->where('type_enum', UserActionTypeEnum::DISLIKE)
                ->count(),
            'suggested' => (bool) $this
                ->userActions
                ->where('user_id', $user->id)
                ->where('type_enum', UserActionTypeEnum::SUGGEST)
                ->count(),
        ];
    }

    public function getStatistics()
    {
        return app(BusinessStatisticsAction::class)->execute($this);
    }

    public function pendingDiscountCampaign(): ?DiscountCampaign
    {
        return $this->discountCampaigns()
            ->where('state_enum', DiscountCampaignStateEnum::PENDING)
            ->first();
    }

    //Elastic
    public function searchableAs()
    {
        return 'business_index';
    }

    public function shouldBeSearchable()
    {
        return !is_null($this->phone);
    }

    public function searchableWith()
    {
        return ['country', 'city', 'state', 'categories', 'logo'];
    }

    public static function defaultLogo()
    {
        return [
            'file_name' => 'logo.png',
            'url' => url('assets/placeholders/logo.png'),
            'thumbnail_url' => url('assets/placeholders/logo.png'),
        ];
    }

    public function toSearchableArray()
    {
        $categories = $this->categories->map(function (Category $category) {
            return [
                'id' => $category->id,
                'name' => $category->getTranslations('name'),
                'parent_ids' => $category
                    ->ancestors
                    ->pluck('id')
                    ->prepend($category->id)
                    ->toArray(),
            ];
        })
        ->toArray();

        $data = [
            'name' => $this->name,
            'business_type' => $this->business_type,
            'country' => [
                'id' => $this->country_id,
                'name' => $this->country?->getTranslations('name'),
                'iso_code' => $this->country?->iso2,
            ],
            'city' => [
                'id' => $this->city_id,
                'name' => $this->city?->name,
                'iso_code' => $this->city?->iso2,
            ],
            'location' => [
                'lat' => $this->latitude,
                'lon' => $this->longitude,
            ],
            'address' => $this->address,
            'categories' => $categories,
            'established_year' => $this->established_year,
            'created_at' => $this->created_at,
            'logo' => $this->logo ?
                MediaResource::make($this->logo?->getFirstMedia($this->logo?->type_enum)) :
                null,
            'ratings' => app(CommentsRatingsStatisticsCalculateAction::class)
                ->execute('businesses', $this->id),
        ];

        return $data;
    }
}
