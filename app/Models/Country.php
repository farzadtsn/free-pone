<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Country extends Model
{
    use HasFactory, HasTranslations;

    protected $guarded = [];

    protected $translatable = ['name'];

    public function getFlagAttribute()
    {
        return config('app.url') . '/assets/images/flags/1x1/' . strtolower($this->iso2) . '.svg';
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function states()
    {
        return $this->hasMany(State::class);
    }
}
