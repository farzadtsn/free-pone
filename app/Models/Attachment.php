<?php

namespace App\Models;

use App\Enums\AttachmentTypeEnum;
use App\Models\QueryBuilders\AttachmentQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Attachment extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $guarded = [];

    protected $with = [
        'media',
    ];

    public function newEloquentBuilder($query): AttachmentQueryBuilder
    {
        return new AttachmentQueryBuilder($query);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
              ->width(150)
              ->height(150);
    }

    public function registerMediaCollections(Media $media = null): void
    {
        $this->addMediaCollection(AttachmentTypeEnum::BUSINESS_GALLERY)
            ->singleFile();

        $this->addMediaCollection(AttachmentTypeEnum::BUSINESS_LOGO)
            ->singleFile()
            ->useFallbackPath(public_path('/assets/placeholders/logo.png'));

        $this->addMediaCollection(AttachmentTypeEnum::DISCOUNT_CAMPAIGN_PHOTO)
            ->singleFile();
    }

    public function owner()
    {
        return $this->morphTo(
            type: 'model_type',
            id: 'model_id',
        );
    }

    public function reports()
    {
        return $this->morphMany(Report::class, 'reportable');
    }
}
