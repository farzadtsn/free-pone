<?php

namespace App\Models;

use App\Models\QueryBuilders\CategoryQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\Translatable\HasTranslations;

class Category extends Model
{
    use HasFactory, HasTranslations, NodeTrait;

    public $translatable = [
        'name',
    ];

    public $guarded = [];

    public function newEloquentBuilder($query): CategoryQueryBuilder
    {
        return new CategoryQueryBuilder($query);
    }

    public function canBeDeleted()
    {
        return !$this->descendants()->exists() || !$this->businesses()->exists();
    }

    public function businesses()
    {
        return $this->belongsToMany(Business::class);
    }
}
