<?php

namespace App\Console\Commands;

use Database\Seeders\CitiesTableChunkFiveSeeder;
use Database\Seeders\CitiesTableChunkFourSeeder;
use Database\Seeders\CitiesTableChunkOneSeeder;
use Database\Seeders\CitiesTableChunkThreeSeeder;
use Database\Seeders\CitiesTableChunkTwoSeeder;
use Database\Seeders\CountryTableSeeder;
use Database\Seeders\StatesTableSeeder;
use Illuminate\Console\Command;

class SeedCountriesAndCitiesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mono:countries';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');
        $this->call('db:seed', ['--class' => CountryTableSeeder::class, '--force' => '', '--no-interaction' => '']);
        $this->call('db:seed', ['--class' => StatesTableSeeder::class, '--force' => '', '--no-interaction' => '']);
        $this->call('db:seed', ['--class' => CitiesTableChunkOneSeeder::class, '--force' => '', '--no-interaction' => '']);
        $this->call('db:seed', ['--class' => CitiesTableChunkTwoSeeder::class, '--force' => '', '--no-interaction' => '']);
        $this->call('db:seed', ['--class' => CitiesTableChunkThreeSeeder::class, '--force' => '', '--no-interaction' => '']);
        $this->call('db:seed', ['--class' => CitiesTableChunkFourSeeder::class, '--force' => '', '--no-interaction' => '']);
        $this->call('db:seed', ['--class' => CitiesTableChunkFiveSeeder::class, '--force' => '', '--no-interaction' => '']);

        return 0;
    }
}
