<?php

namespace App\Console\Commands;

use App\Models\Business;
use Illuminate\Console\Command;

class ReopenBusinessesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mono:reopen-business';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reopen Businesses';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Business::query()
            ->where('is_closed', true)
            ->where('reopen_date', now())
            ->update([
                'is_closed' => false,
                'reopen_date' => null,
            ]);

        return 0;
    }
}
