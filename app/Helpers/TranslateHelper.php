<?php

namespace App\Helpers;

class TranslateHelper
{
    public static function getTranslation(?array $data)
    {
        return $data[app()->getLocale()] ?? $data[config('app.locale')] ?? '';
    }
}
