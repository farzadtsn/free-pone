<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (ValidationException $e, $request) {
            return Response::error(
                code: $e->status,
                message: trans('messages.validation_failed'),
                data: $e->errors(),
            );
        });

        $this->renderable(function (NotFoundHttpException $e, $request) {
            return Response::error(
                code: HttpFoundationResponse::HTTP_NOT_FOUND,
                message: trans('messages.http_not_found'),
            );
        });

        $this->renderable(function (AuthenticationException $e, $request) {
            return Response::error(
                code: HttpFoundationResponse::HTTP_UNAUTHORIZED,
                message: $e->getMessage() ?: trans('messages.unauthenticated'),
            );
        });

        $this->renderable(function (UnauthorizedException $e, $request) {
            return Response::error(
                code: HttpFoundationResponse::HTTP_FORBIDDEN,
                message: $e->getMessage() ?: trans('messages.unauthorized'),
            );
        });

        $this->renderable(function (ReadOnlyException $e, $request) {
            return Response::error(
                code: HttpFoundationResponse::HTTP_FORBIDDEN,
                message: $e->getMessage() ?: trans('messages.the_model_is_read_only'),
            );
        });

        if (!App::environment('local')) {
            $this->renderable(function (ReadOnlyException $e, $request) {
                return Response::error(
                    code: $e->getCode(),
                    message: $e->getMessage(),
                );
            });
        }
    }
}
