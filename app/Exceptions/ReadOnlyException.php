<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;

class ReadOnlyException extends Exception
{
    public function __construct(Exception $previous = null)
    {
        parent::__construct(
            trans('model_is_read_only'),
            Response::HTTP_UNPROCESSABLE_ENTITY,
            $previous
        );
    }
}
