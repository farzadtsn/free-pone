<?php

namespace App\Enums;

class AttachmentTypeEnum extends BaseEnum
{
    const BUSINESS_GALLERY = 'business_gallery';
    const BUSINESS_LOGO = 'business_logo';
    const DISCOUNT_CAMPAIGN_PHOTO = 'discount_campaign_photo';
}
