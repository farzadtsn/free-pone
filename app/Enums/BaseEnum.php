<?php

namespace App\Enums;

use ReflectionClass;

class BaseEnum
{
    public static function getValues()
    {
        return array_values((new ReflectionClass(static::class))->getConstants());
    }

    public static function labels()
    {
        $keys = self::getValues();
        $translatedValues = [];
        foreach ($keys as $key) {
            $translatedValues[$key] = trans('labels.' . $key);
        }

        return array_combine($keys, $translatedValues);
    }
}
