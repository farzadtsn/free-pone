<?php

namespace App\Enums;

class UserActionTypeEnum extends BaseEnum
{
    const LIKE = 'like';
    const DISLIKE = 'dislike';
    const SUGGEST = 'suggest';

    public static function positiveActions()
    {
        return [
            self::LIKE,
            self::SUGGEST,
        ];
    }

    public static function commentActions()
    {
        return [
            self::LIKE,
            self::DISLIKE,
        ];
    }
}
