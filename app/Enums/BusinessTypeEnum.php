<?php

namespace App\Enums;

class BusinessTypeEnum extends BaseEnum
{
    const SOLO_PROVIDER = 'solo_provider';
    const INDEPENDENT_CONTRACTOR = 'independent_contractor';
    const COMPANY = 'company';
    const THIRD_PARTY = 'third_party';
}
