<?php

namespace App\Enums;

class CommentSortEnum extends SortEnum
{
    const RATING = 'rating';
    const LIKES_COUNT = 'likes_count';
    const DISLIKES_COUNT = 'dislikes_count';
}
