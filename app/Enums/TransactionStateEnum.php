<?php

namespace App\Enums;

class TransactionStateEnum extends BaseEnum
{
    const PENDING = 'pending';
    const SUCCEED = 'succeed';
    const FAILED = 'failed';
    const CANCELED = 'canceled';

    public static function color(string $state): string
    {
        return match ($state) {
            self::PENDING => '#ffc107',
            self::SUCCEED => '#28a745',
            self::FAILED => '#dc3545',
            self::CANCELED => '#6c757d',
            default => '#6c757d',
        };
    }
}
