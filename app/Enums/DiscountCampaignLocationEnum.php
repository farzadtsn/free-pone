<?php

namespace App\Enums;

class DiscountCampaignLocationEnum extends BaseEnum
{
    const ONLINE = 'online';
    const CUSTOMER_LOCATION = 'customer_location';
    const BUSINESS_LOCATION = 'business_location';
}
