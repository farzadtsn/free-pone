<?php

namespace App\Enums;

class HtmlInputTypeEnum extends BaseEnum
{
    const SELECT = 'select';
    const MULTISELECT = 'multiselect';
    const CHECKBOX = 'checkbox';
    const RADIOBUTTON = 'radiobutton';
}
