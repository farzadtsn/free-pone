<?php

namespace App\Enums;

class PhotoReportEnum extends BaseEnum
{
    const IRRELEVANT = 'irrelevant';
    const INTELLECTUAL_VIOLATION = 'intellectual_violation';
    const VIOLENCE = 'violence';
    const FRAUD = 'fraud';
}
