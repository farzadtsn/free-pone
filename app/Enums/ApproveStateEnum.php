<?php

namespace App\Enums;

class ApproveStateEnum extends BaseEnum
{
    const PENDING = 'pending';
    const APPROVED = 'approved';
    const REJECTED = 'rejected';
}
