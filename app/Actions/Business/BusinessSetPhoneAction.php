<?php

namespace App\Actions\Business;

use App\Data\Business\BusinessPhoneData;
use App\Models\User;

class BusinessSetPhoneAction
{
    private int $step = 3;

    public function __construct(
        private BusinessValidateStepAction $businessValidateStepAction,
    ) {
    }

    public function execute(BusinessPhoneData $businessPhoneData, User $user): User
    {
        $this->businessValidateStepAction->execute($user, $this->step);

        $user->business
            ->update([
                'phone' => $businessPhoneData->phone,
                'step' => null,
            ]);

        return $user;
    }
}
