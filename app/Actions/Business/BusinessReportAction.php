<?php

namespace App\Actions\Business;

use App\Data\Report\ReportData;
use App\Models\Business;
use App\Models\Report;
use Illuminate\Validation\UnauthorizedException;

class BusinessReportAction
{
    public function execute(
        ReportData $businessReportData,
        Business $business
    ): Report {
        if ($business->user_id === auth()->id()) {
            throw new UnauthorizedException(
                trans('messages.cant_report_yourself'),
            );
        }

        if ($business->reports()->where('user_id', auth()->id())->exists()) {
            throw new UnauthorizedException(
                trans('messages.already_reported')
            );
        }

        return $business
            ->reports()
            ->create([
                'subject' => $businessReportData->subject,
                'description' => $businessReportData->description,
                'user_id' => auth()->id(),
            ]);
    }
}
