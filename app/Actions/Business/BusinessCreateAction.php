<?php

namespace App\Actions\Business;

use App\Data\Business\BusinessCreateData;
use App\Models\Business;

class BusinessCreateAction
{
    public function execute(BusinessCreateData $businessCreateData): Business
    {
        return $businessCreateData
            ->user
            ->business()
            ->create([]);
    }
}
