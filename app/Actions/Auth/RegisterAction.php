<?php

namespace App\Actions\Auth;

use App\Data\Auth\LoginData;
use App\Data\Auth\RegisterData;
use App\Http\Resources\Auth\UserResource;
use App\Models\User;
use Hash;
use Illuminate\Support\Facades\DB;

class RegisterAction
{
    public function __construct(
        private GetTokenAction $getTokenAction,
    ) {
    }

    public function execute(RegisterData $data): array
    {
        //ToDo make request to auth microservice and get token
        DB::table(config('database.user_schema') . '.users')->insert([
            'name'=> $data->name,
            'email'=> $data->email,
            'email_verified_at' => now(),
            'password'=> Hash::make($data->password),
        ]);

        return [
            'token' => $this->getTokenAction->execute(LoginData::from([
                'email' => $data->email,
                'password' => $data->password,
            ])),
            'user'  => UserResource::make(User::where('email', $data->email)->first()),
        ];
    }
}
