<?php

namespace App\Actions\Auth;

use App\Data\Auth\LoginData;
use App\Http\Resources\Auth\UserResource;
use App\Models\User;

class LoginAction
{
    public function __construct(
        private GetTokenAction $getTokenAction,
    ) {
    }

    public function execute(LoginData $data): array
    {
        $token = $this->getTokenAction->execute($data);

        $user = User::where('email', $data->email)->with('business')->firstOrFail();

        return [
            'token' => $token,
            'user'  => UserResource::make($user),
        ];
    }
}
