<?php

namespace App\Actions\Auth;

use App\Data\Auth\LoginData;
use Illuminate\Auth\AuthenticationException;
use Monopone\Auth\AuthService;

class GetTokenAction
{
    public function __construct(
        private AuthService $authService
    ) {
    }

    public function execute(LoginData $data): string
    {
        $response = $this->authService->login($data->email, $data->password)->json();

        if (!isset($response['token'])) {
            throw new AuthenticationException(
                $response['message'] ?? trans('messages.something_went_wrong'),
            );
        }

        return $response['token'];
    }
}
