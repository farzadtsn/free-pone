<?php

namespace App\Actions\DiscountCampaign;

use App\Data\DiscountCampaign\DiscountCampaignAppointmentData;
use App\Data\DiscountCampaign\DiscountCampaignDescriptionData;
use App\Data\DiscountCampaign\DiscountCampaignLocationData;
use App\Models\Business;
use App\Models\DiscountCampaign;

class DiscountCampaignSetAppointmentRequiredAction
{
    private int $step = 7;

    public function __construct(
        private DiscountCampaignCreateAction $discountCampaignCreateAction,
        private DiscountCampaignValidateStepAction $discountCampaignValidateStepAction
    ) {
    }

    public function execute(
        DiscountCampaignAppointmentData $data,
        Business $business,
    ): DiscountCampaign {
        $discountCampaign = $this->discountCampaignCreateAction->execute($business);

        $this->discountCampaignValidateStepAction->execute(
            $discountCampaign,
            $this->step,
        );

        $discountCampaign->update([
            'step' => $this->step + 1,
            'is_appointment_required' => $data->is_appointment_required,
        ]);

        return $discountCampaign;
    }
}
