<?php

namespace App\Actions\DiscountCampaign;

use App\Data\DiscountCampaign\DiscountCampaignTitleData;
use App\Models\Business;
use App\Models\DiscountCampaign;

class DiscountCampaignSetTitleAction
{
    private int $step = 2;

    public function __construct(
        private DiscountCampaignCreateAction $discountCampaignCreateAction,
        private DiscountCampaignValidateStepAction $discountCampaignValidateStepAction
    ) {
    }

    public function execute(
        DiscountCampaignTitleData $discountCampaignTitleData,
        Business $business,
    ): DiscountCampaign {
        $discountCampaign = $this->discountCampaignCreateAction->execute($business);

        $this->discountCampaignValidateStepAction->execute(
            $discountCampaign,
            $this->step,
        );

        $discountCampaign->update([
            'step' => $this->step + 1,
            'name' => $discountCampaignTitleData->name,
        ]);

        return $discountCampaign;
    }
}
