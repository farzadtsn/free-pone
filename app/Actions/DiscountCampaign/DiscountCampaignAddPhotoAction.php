<?php

namespace App\Actions\DiscountCampaign;

use App\Data\DiscountCampaign\DiscountCampaignPhotoData;
use App\Enums\AttachmentTypeEnum;
use App\Models\Business;
use App\Models\DiscountCampaign;

class DiscountCampaignAddPhotoAction
{
    private int $step = 4;

    public function __construct(
        private DiscountCampaignCreateAction $discountCampaignCreateAction,
        private DiscountCampaignValidateStepAction $discountCampaignValidateStepAction
    ) {
    }

    public function execute(
        DiscountCampaignPhotoData $data,
        Business $business,
    ): DiscountCampaign {
        $discountCampaign = $this->discountCampaignCreateAction->execute($business);

        $this->discountCampaignValidateStepAction->execute(
            $discountCampaign,
            $this->step,
        );

        $attachment = $discountCampaign
            ->photos()
            ->create([
                'type_enum' => AttachmentTypeEnum::DISCOUNT_CAMPAIGN_PHOTO,
            ]);

        $attachment->addMedia($data->photo)
            ->toMediaCollection(AttachmentTypeEnum::DISCOUNT_CAMPAIGN_PHOTO);

        $discountCampaign->update([
            'step' => $this->step + 1,
        ]);

        return $discountCampaign;
    }
}
