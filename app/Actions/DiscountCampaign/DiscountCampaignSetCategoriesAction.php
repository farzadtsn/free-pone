<?php

namespace App\Actions\DiscountCampaign;

use App\Data\DiscountCampaign\DiscountCampaignCategoriesData;
use App\Models\Business;
use App\Models\DiscountCampaign;

class DiscountCampaignSetCategoriesAction
{
    public function __construct(
        private DiscountCampaignCreateAction $discountCampaignCreateAction,
    ) {
    }

    public function execute(
        DiscountCampaignCategoriesData $discountCampaignData,
        Business $business,
    ): DiscountCampaign {
        $discountCampaign = $this->discountCampaignCreateAction->execute($business);

        $discountCampaign->categories()->sync(
            $discountCampaignData->categories
        );

        $discountCampaign->update(['step' => 2]);

        return $discountCampaign;
    }
}
