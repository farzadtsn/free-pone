<?php

namespace App\Actions\DiscountCampaign;

use App\Data\DiscountCampaign\DiscountCampaignDescriptionData;
use App\Models\Business;
use App\Models\DiscountCampaign;

class DiscountCampaignSetDescriptionAction
{
    private int $step = 3;

    public function __construct(
        private DiscountCampaignCreateAction $discountCampaignCreateAction,
        private DiscountCampaignValidateStepAction $discountCampaignValidateStepAction
    ) {
    }

    public function execute(
        DiscountCampaignDescriptionData $data,
        Business $business,
    ): DiscountCampaign {
        $discountCampaign = $this->discountCampaignCreateAction->execute($business);

        $this->discountCampaignValidateStepAction->execute(
            $discountCampaign,
            $this->step,
        );

        $discountCampaign->update([
            'step' => $this->step + 1,
            'description' => $data->description,
        ]);

        return $discountCampaign;
    }
}
