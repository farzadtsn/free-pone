<?php

namespace App\Actions\DiscountCampaign;

use App\Models\Business;
use App\Models\DiscountCampaign;

class DiscountCampaignCreateAction
{
    public function execute(Business $business): DiscountCampaign
    {
        return $business->pendingDiscountCampaign() ?: $business->discountCampaigns()->create([]);
    }
}
