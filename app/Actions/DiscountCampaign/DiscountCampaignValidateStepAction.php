<?php

namespace App\Actions\DiscountCampaign;

use App\Models\DiscountCampaign;
use Illuminate\Validation\UnauthorizedException;

class DiscountCampaignValidateStepAction
{
    public function execute(DiscountCampaign $discountCampaign, int $step): void
    {
        if ($discountCampaign->step && $discountCampaign->step < $step) {
            throw new UnauthorizedException(
                message: trans('messages.previous_steps_are_not_complete'),
            );
        }
    }
}
