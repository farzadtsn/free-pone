<?php

namespace App\Actions\DiscountCampaign;

use App\Data\DiscountCampaign\DiscountCampaignDescriptionData;
use App\Data\DiscountCampaign\DiscountCampaignLocationData;
use App\Models\Business;
use App\Models\DiscountCampaign;

class DiscountCampaignSetLocationAction
{
    private int $step = 6;

    public function __construct(
        private DiscountCampaignCreateAction $discountCampaignCreateAction,
        private DiscountCampaignValidateStepAction $discountCampaignValidateStepAction
    ) {
    }

    public function execute(
        DiscountCampaignLocationData $data,
        Business $business,
    ): DiscountCampaign {
        $discountCampaign = $this->discountCampaignCreateAction->execute($business);

        $this->discountCampaignValidateStepAction->execute(
            $discountCampaign,
            $this->step,
        );

        $discountCampaign->update([
            'step' => $this->step + 1,
            'location_enum' => $data->location,
        ]);

        return $discountCampaign;
    }
}
