<?php

namespace App\Actions\Currency;

use App\Data\Currency\CurrencyCreateData;
use App\Models\Currency;

class CurrencyCreateAction
{
    public function execute(CurrencyCreateData $data): Currency
    {
        return Currency::create([
            'name' => $data->name,
            'symbol' => $data->symbol,
            'iso_code' => $data->iso_code,
            'exchange_rate' => $data->exchange_rate,
        ]);
    }
}
