<?php

namespace App\Actions\Currency;

use App\Data\Currency\CurrencyUpdateData;
use App\Models\Currency;

class CurrencyUpdateAction
{
    public function execute(CurrencyUpdateData $data, Currency $currency): Currency
    {
        $currency->update([
            'name' => $data->name,
            'symbol' => $data->symbol,
            'iso_code' => $data->iso_code,
            'exchange_rate' => $data->exchange_rate,
        ]);

        return $currency;
    }
}
