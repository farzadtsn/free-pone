<?php

namespace App\Actions\Comment;

use App\Models\Comment;

class CommentsRatingsStatisticsCalculateAction
{
    public function execute(string $type, int $id): array
    {
        return [
            'average' => round(
                Comment::query()->for($type, $id)->avg('rating'),
                1
            ),
            'count' => Comment::query()
                ->for($type, $id)
                ->count(),
        ];
    }
}
