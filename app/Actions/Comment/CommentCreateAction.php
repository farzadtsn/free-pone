<?php

namespace App\Actions\Comment;

use App\Data\Comment\CommentCreateData;
use App\Models\Business;
use App\Models\User;

class CommentCreateAction
{
    public function execute(
        CommentCreateData $commentCreateData,
        Business $commentable,
        User $user,
    ):void {
        $this->canCreateComment($commentable, $user);

        $commentable->comments()->create([
            'body' => $commentCreateData->body,
            'rating' => $commentCreateData->rating,
            'user_id' => $user->id,
        ]);
    }

    private function canCreateComment(
        Business $commentable,
        User $user,
    ):void {
    }
}
