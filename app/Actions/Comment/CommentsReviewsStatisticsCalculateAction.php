<?php

namespace App\Actions\Comment;

use App\Models\Comment;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CommentsReviewsStatisticsCalculateAction
{
    public function execute(string $type, int $id): array
    {
        $ratingsCount = Comment::query()
            ->for($type, $id)
            ->select(DB::raw('count(*) as rating_count, rating'))
            ->groupBy('rating')
            ->get();

        $total = $ratingsCount->sum('rating_count');

        return [
            [
                'name' => 'positive',
                'description' => '4 starts and above',
                'percent' => $this->getPercentage($ratingsCount, $total, [4, 5]),
            ],
            [
                'name' => 'Neutral',
                'description' => '3 starts',
                'percent' => $this->getPercentage($ratingsCount, $total, [3]),
            ],
            [
                'name' => 'Negative',
                'description' => 'Under 3 starts',
                'percent' => $this->getPercentage($ratingsCount, $total, [1, 2]),
            ],
        ];
    }

    private function getPercentage(Collection $ratingsCount, int $total, array $ratings): float
    {
        if ($total === 0) {
            return 0;
        }

        return round(
            $ratingsCount->whereIn('rating', $ratings)->sum('rating_count') / $total * 100,
            0
        );
    }
}
