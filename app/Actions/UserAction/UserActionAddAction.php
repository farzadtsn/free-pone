<?php

namespace App\Actions\UserAction;

use App\Enums\UserActionTypeEnum;
use App\Models\Business;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Validation\UnauthorizedException;

class UserActionAddAction
{
    public function execute(
        Business|Comment $actionable,
        User $user,
        string $action
    ):void {
        $this->canDoThisAction($actionable, $user, $action);

        $actionable->userActions()->create([
            'type_enum' => $action,
            'user_id' => $user->id,
        ]);
    }

    private function canDoThisAction(
        Business|Comment $actionable,
        User $user,
        string $action
    ):void {
        if ($actionable
            ->userActions()
            ->where('type_enum', $action)
            ->where('user_id', $user->id)
            ->exists()) {
            throw new UnauthorizedException(
                trans('messages.action_already_done')
            );
        }

        if ($action === UserActionTypeEnum::DISLIKE &&
            $actionable->userActions()
                ->where('user_id', $user->id)
                ->whereIn('type_enum', UserActionTypeEnum::positiveActions())
                ->exists()) {
            throw new UnauthorizedException(
                trans('messages.liked_or_suggested_previously')
            );
        }

        if (in_array($action, UserActionTypeEnum::positiveActions(), true) &&
            $actionable->userActions()
                ->where('user_id', $user->id)
                ->where('type_enum', UserActionTypeEnum::DISLIKE)
                ->exists()) {
            throw new UnauthorizedException(
                trans('messages.disliked_previously')
            );
        }
    }
}
