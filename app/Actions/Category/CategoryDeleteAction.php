<?php

namespace App\Actions\Category;

use App\Exceptions\ReadOnlyException;
use App\Models\Category;

class CategoryDeleteAction
{
    public function execute(Category $category): void
    {
        if (!$category->canBeDeleted()) {
            throw new ReadOnlyException();
        }

        $category->delete();
    }
}
