<?php

namespace App\Actions\Category;

use App\Data\Category\CategoryUpdateData;
use App\Models\Category;

class CategoryUpdateAction
{
    public function execute(CategoryUpdateData $data, Category $category): Category
    {
        $category
            ->setTranslation('name', $data->locale, $data->name)
            ->save();

        return $category;
    }
}
