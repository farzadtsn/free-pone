<?php

namespace App\Actions\Category;

use App\Data\Category\CategoryCreateData;
use App\Models\Category;

class CategoryCreateAction
{
    public function execute(CategoryCreateData $data): Category
    {
        return Category::create([
            'name' => $data->name,
            'parent_id' => $data->parent_id,
        ]);
    }
}
