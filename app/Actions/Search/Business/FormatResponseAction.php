<?php

namespace App\Actions\Search\Business;

use App\Data\Business\BusinessSearchData;
use Elastic\ScoutDriverPlus\Decorators\SearchResult;
use Elastic\ScoutDriverPlus\Paginator;

class FormatResponseAction
{
    public function __construct()
    {
    }

    public function execute(Paginator|SearchResult $searchResult): array
    {
        $documents = $searchResult->documents();

        foreach ($documents as $document) {
            $results[] = BusinessSearchData::from($document->content())->formattedResponse();
        }

        return $results ?? [];
    }

    public function getLocaleName(array $names): string
    {
        $locale = app()->getLocale();

        return $names[$locale] ?? $names[config('app.locale')] ?? '';
    }
}
