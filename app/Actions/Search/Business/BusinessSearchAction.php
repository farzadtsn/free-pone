<?php

namespace App\Actions\Search\Business;

use App\Actions\Search\SearchAbstract;
use App\Models\Business;

class BusinessSearchAction extends SearchAbstract
{
    protected $model = Business::class;

    public function __construct(
        protected ApplyFilersAction $applyFiltersAction,
        protected ApplySortAction $applySortAction,
        protected ApplyAggregationAction $applyAggregationAction,
        protected FormatResponseAction $formatResponseAction,
    ) {
    }
}
