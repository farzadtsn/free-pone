<?php

namespace App\Actions\Search\DiscountCampaign;

use Elastic\ScoutDriverPlus\Builders\SearchParametersBuilder;

class ApplySortAction
{
    public function __construct()
    {
    }

    public function execute(SearchParametersBuilder $searchRequestBuilder, ?array $sort): SearchParametersBuilder
    {
        if (!$sort) {
            return $searchRequestBuilder;
        }

        [$sortField, $sortDirection] = $sort;

        $sortField = match ($sortField) {
            'registered_at' => 'business.created_at',
        };

        return $searchRequestBuilder->sort($sortField, $sortDirection);
    }
}
