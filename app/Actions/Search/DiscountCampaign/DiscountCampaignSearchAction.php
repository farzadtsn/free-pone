<?php

namespace App\Actions\Search\DiscountCampaign;

use App\Actions\Search\SearchAbstract;
use App\Models\DiscountCampaign;

class DiscountCampaignSearchAction extends SearchAbstract
{
    protected $model = DiscountCampaign::class;

    public function __construct(
        protected ApplyFilersAction $applyFiltersAction,
        protected ApplySortAction $applySortAction,
        protected ApplyAggregationAction $applyAggregationAction,
        protected FormatResponseAction $formatResponseAction,
    ) {
    }
}
