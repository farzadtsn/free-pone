<?php

namespace App\Actions\Search\DiscountCampaign;

use App\Data\Business\BusinessFilterData;
use Elastic\ScoutDriverPlus\Builders\BoolQueryBuilder;
use Elastic\ScoutDriverPlus\Support\Query;

class ApplyFilersAction
{
    private BusinessFilterData $filters;

    public function execute(BusinessFilterData $filters): ?BoolQueryBuilder
    {
        $this->filters = $filters;

        // if (count(array_filter($filters->all())) === 0) {
        //     return null;
        // }

        $query = Query::bool();

        $query = $this->searchTerm($query);
        $query = $this->filterCountry($query);
        $query = $this->filterCountryByIsoCode($query);
        $query = $this->filterCity($query);
        $query = $this->filterCategories($query);
        $query = $this->filterLocation($query);
        $query = $this->filterRating($query);
        $query = $this->filterNested($query);

        return $query;
    }

    private function searchTerm(BoolQueryBuilder $query): BoolQueryBuilder
    {
        if ($this->filters->q) {
            $query->must(
                Query::wildcard()
                    ->field('name')
                    ->value(strtolower($this->filters->q) . '*')
            );
        }

        return $query;
    }

    private function filterCountry(BoolQueryBuilder $query): BoolQueryBuilder
    {
        if ($this->filters->countries) {
            $query->must(
                Query::terms()
                    ->field('business.country.id')
                    ->values(array_values($this->filters->countries))
            );
        }

        return $query;
    }

    private function filterCountryByIsoCode(BoolQueryBuilder $query): BoolQueryBuilder
    {
        if ($this->filters->country_code) {
            $query->must(
                Query::term()
                    ->field('business.country.iso_code')
                    ->value($this->filters->country_code)
            );
        }

        return $query;
    }

    private function filterCity(BoolQueryBuilder $query): BoolQueryBuilder
    {
        if ($this->filters->cities) {
            $query->must(
                Query::terms()
                    ->field('business.city.id')
                    ->values(array_values($this->filters->cities))
            );
        }

        return $query;
    }

    private function filterCategories(BoolQueryBuilder $query): BoolQueryBuilder
    {
        if ($this->filters->categories) {
            $query->must(
                Query::nested()
                    ->path('categories')
                    ->query(
                        Query::terms()
                            ->field('categories.parent_ids')
                            ->values($this->filters->categories)
                    )
            );
        }

        return $query;
    }

    private function filterLocation(BoolQueryBuilder $query): BoolQueryBuilder
    {
        if ($this->filters->location) {
            $query->must(
                Query::geoDistance()
                    ->field('business.location')
                    ->distance($this->filters->location->distance . 'mi')
                    ->lat($this->filters->location->lat)
                    ->lon($this->filters->location->long)
                    ->validationMethod('IGNORE_MALFORMED')
            );
        }

        return $query;
    }

    private function filterRating(BoolQueryBuilder $query): BoolQueryBuilder
    {
        if ($this->filters->rating) {
            $query->must(
                Query::range()
                    ->field('business.ratings.average')
                    ->gte($this->filters->rating)
            );
        }

        return $query;
    }

    private function filterNested(BoolQueryBuilder $query): BoolQueryBuilder
    {
        $query->must(
            Query::nested()
                ->path('variants')
                ->query(
                    Query::bool()->must(
                        Query::range()
                            ->field('variants.discounted_price')
                            ->gte($this->filters->min_price ?: 0)
                            ->when($this->filters->max_price, function ($query, $maxPrice) {
                                $query->lte($maxPrice ?: 0);
                            })
                    )->when(isset($this->filters->in_stock), function ($query) {
                        $query->must(
                            Query::term()
                                ->field('variants.in_stock')
                                ->value($this->filters->in_stock)
                        );
                    })
                )
                ->innerHits([
                    'size' => 1,
                    'sort' => [
                        'variants.in_stock' => 'desc',
                        'variants.discounted_price' => 'asc',
                    ],
                ])
                ->ignoreUnmapped(true)
        );

        return $query;
    }
}
