<?php

namespace App\Actions\Search\DiscountCampaign;

use ElasticScoutDriverPlus\Builders\SearchParametersBuilder;

class ApplyAggregationAction
{
    public function __construct()
    {
    }

    public function execute(SearchParametersBuilder $searchRequestBuilder): SearchParametersBuilder
    {
        return $searchRequestBuilder->aggregate('max_price', [
            'max' => [
                'field' => 'wage',
            ],
        ])->aggregate('min_price', [
            'min' => [
                'field' => 'wage',
            ],
        ]);
    }
}
