<?php

namespace App\Actions\Search;

use App\Data\Filter\CommonFiltersData;
use Elastic\ScoutDriverPlus\Builders\SearchParametersBuilder;
use Elastic\ScoutDriverPlus\Decorators\SearchResult;
use Elastic\ScoutDriverPlus\Paginator;
use Spatie\LaravelData\Data;

class SearchAbstract
{
    public function execute(
        Data $parameters,
        CommonFiltersData $commonFilters,
        bool $paginate = true
    ): Paginator|SearchResult {
        $searchQuery = $this->model::searchQuery(
            query: $this->applyFiltersAction->execute($parameters)
        );

        $searchQuery = $this->applySortAction->execute(
            $searchQuery,
            $parameters->sort ?? null
        );

        // $searchQuery = $this->applyAggregationAction->execute($searchQuery);

        $searchResults = $this->paginate($searchQuery, $commonFilters, $paginate);

        return $searchResults;
    }

    private function paginate(
        SearchParametersBuilder $searchQuery,
        CommonFiltersData $filters,
        bool $paginate
    ): Paginator|SearchResult {
        if ($paginate) {
            return $searchQuery->paginate(
                $filters->size,
                'page',
                $filters->page
            );
        }

        return $searchQuery->size($filters->size)->execute();
    }
}
