<?php

namespace App\Actions\Search;

class SearchResponse
{
    public array $aggregations;
    public array $results;
    public array $pagination;
}
