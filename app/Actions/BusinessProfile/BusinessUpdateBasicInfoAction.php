<?php

namespace App\Actions\BusinessProfile;

use App\Data\BusinessProfile\BusinessBasicInfoData;
use App\Models\User;

class BusinessUpdateBasicInfoAction
{
    public function __construct(
        private BusinessUpdateLogoAction $businessUpdateLogoAction
    ) {
    }

    public function execute(BusinessBasicInfoData $businessInfoData, User $user): User
    {
        $user->business
            ->update([
                'name' => $businessInfoData->business_name,
                'phone' => $businessInfoData->phone,
                'website' => $businessInfoData->website,
            ]);

        $user = $this->businessUpdateLogoAction->execute(
            $businessInfoData->logo,
            $user
        );

        return $user;
    }
}
