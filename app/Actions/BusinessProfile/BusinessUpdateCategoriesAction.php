<?php

namespace App\Actions\BusinessProfile;

use App\Data\BusinessProfile\BusinessCategoriesData;
use App\Models\User;

class BusinessUpdateCategoriesAction
{
    public function execute(
        BusinessCategoriesData $businessCategoriesData,
        User $user
    ): User {
        $user->business->categories()->sync(
            $businessCategoriesData->categories,
        );

        return $user;
    }
}
