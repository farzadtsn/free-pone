<?php

namespace App\Actions\BusinessProfile;

use App\Enums\AttachmentTypeEnum;
use App\Models\User;
use Illuminate\Http\UploadedFile;

class BusinessUpdateLogoAction
{
    public function execute(UploadedFile $logo, User $user): User
    {
        $attachment = $user->business
            ->logo()
            ->updateOrCreate(
                ['type_enum' => AttachmentTypeEnum::BUSINESS_LOGO],
                []
            );

        $attachment->addMedia($logo)
            ->toMediaCollection(AttachmentTypeEnum::BUSINESS_LOGO);

        return $user;
    }
}
