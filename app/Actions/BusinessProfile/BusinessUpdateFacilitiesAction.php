<?php

namespace App\Actions\BusinessProfile;

use App\Data\BusinessProfile\BusinessFacilitiesData;
use App\Models\User;

class BusinessUpdateFacilitiesAction
{
    public function execute(
        BusinessFacilitiesData $businessFacilitiesData,
        User $user
    ): User {
        $user->business->facilities()->sync(
            $businessFacilitiesData->facilities,
        );

        return $user;
    }
}
