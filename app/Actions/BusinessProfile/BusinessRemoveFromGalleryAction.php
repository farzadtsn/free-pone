<?php

namespace App\Actions\BusinessProfile;

use App\Models\Attachment;
use App\Models\User;
use Illuminate\Validation\UnauthorizedException;

class BusinessRemoveFromGalleryAction
{
    public function execute(Attachment $attachment, User $user): User
    {
        if (!$attachment->owner?->is($user->business)) {
            throw new UnauthorizedException();
        }

        $attachment->delete();

        return $user;
    }
}
