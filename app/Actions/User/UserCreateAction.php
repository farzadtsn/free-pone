<?php

namespace App\Actions\User;

use App\Data\User\UserCreateData;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserCreateAction
{
    public function execute(UserCreateData $data): User
    {
        DB::table(config('database.user_schema') . '.users')->insert([
            'name'=> $data->name,
            'email'=> $data->email,
            'email_verified_at' => now(),
            'password'=> Hash::make($data->password),
            'name' => $data->name,
        ]);

        return User::where('email', $data->email)->first();
    }
}
