<?php

namespace App\Actions\Attachment;

use App\Data\Attachment\AttachmentRejectData;
use App\Enums\ApproveStateEnum;
use App\Models\Attachment;

class AttachmentRejectAction
{
    public function execute(AttachmentRejectData $attachmentRejectData, Attachment $attachment): Attachment
    {
        $attachment->update([
            'state_enum' => ApproveStateEnum::REJECTED,
            'reject_reason' => $attachmentRejectData->reject_reason,
        ]);

        return $attachment;
    }
}
