<?php

namespace App\Actions\Attachment;

use App\Data\Report\ReportData;
use App\Models\Attachment;
use App\Models\Report;
use Illuminate\Validation\UnauthorizedException;

class AttachmentReportAction
{
    public function execute(
        ReportData $businessReportData,
        Attachment $attachment
    ): Report {
        if ($attachment->reports()->where('user_id', auth()->id())->exists()) {
            throw new UnauthorizedException(
                trans('messages.already_reported')
            );
        }

        return $attachment
            ->reports()
            ->create([
                'subject' => $businessReportData->subject,
                'description' => $businessReportData->description,
                'user_id' => auth()->id(),
            ]);
    }
}
