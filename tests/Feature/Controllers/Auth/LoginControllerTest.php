<?php

namespace Tests\Feature\Controllers\Auth;

// use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    public function test_user_can_login()
    {
        Http::fake([
            config('auth-service.service-url') . '/api/auth/login' => Http::response(
                json_decode(file_get_contents(base_path('tests/data/login.json')), true),
                Response::HTTP_OK,
            ),
        ]);

        $user = User::factory()->create();
        $response = $this->post('api/auth/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);

        $response->assertStatus(200);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('data.token')
            ->has('data.user.email')
            ->etc()
        );
    }
}
