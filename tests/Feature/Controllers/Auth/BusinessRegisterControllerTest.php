<?php

namespace Tests\Feature\Controllers\Auth;

// use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class BusinessRegisterControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function test_business_can_register()
    {
        $response = $this->post('api/auth/business/register', [
            'email' => $email = 'foo@bar.com',
            'password' => 'Abc@1234',
            'first_name' => 'Foo',
            'last_name' => 'Bar',
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('users', [
            'email' => $email,
        ]);

        $this->assertDatabaseHas('businesses', [
            'user_id' => User::where('email', $email)->first()->id,
        ]);

        $response->assertJson(
            fn (AssertableJson $json) => $json->has('data.token')
                ->has('data.user.email')
                ->etc()
        );
    }
}
