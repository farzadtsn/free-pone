<?php

namespace Tests\Feature\Controllers\Auth;

// use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class RegisterControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function test_user_can_register()
    {
        $inputs = User::factory()->make()->toArray();

        $response = $this->post(
            'api/auth/register',
            array_merge(
                $inputs,
                [
                    'name' => 'foo bar',
                    'password' => 'Aa123456@',
                ]
            )
        );

        $response->assertStatus(200);
        $response->assertJson(
            fn (AssertableJson $json) => $json->has('data.token')
            ->has('data.user.email')
            ->etc()
        );
    }
}
