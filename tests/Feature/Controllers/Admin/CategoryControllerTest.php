<?php

namespace Tests\Feature\Controllers\Admin;

use App\Models\Category;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function test_can_admin_create_category()
    {
        $response = $this->post('api/admin/categories', [
            'name' => $name = 'test category',
            'parent_id' => $parent_id = null,
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('categories', [
            'name->en' => $name,
            'parent_id' => $parent_id,
        ]);
    }

    public function test_can_admin_update_category()
    {
        $category = Category::create([
            'name' => 'test category',
        ]);

        $response = $this->put("api/admin/categories/{$category->id}", [
            'name' => $name = 'تست دسته',
            'locale' => 'fa',
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('categories', [
            'name->fa' => $name,
        ]);
    }

    public function test_can_admin_delete_category()
    {
        $category = Category::create([
            'name' => 'test category',
        ]);

        $response = $this->delete("api/admin/categories/{$category->id}");

        $response->assertStatus(200);

        $this->assertDatabaseMissing('categories', [
            'id' => $category->id,
        ]);
    }
}
