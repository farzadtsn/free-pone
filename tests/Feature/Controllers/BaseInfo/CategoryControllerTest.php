<?php

namespace Tests\Feature\Controllers\BaseInfo;

use App\Models\Category;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function test_can_view_category_tree()
    {
        Category::create([
            'name' => 'test category',
        ]);

        $response = $this->get('api/base-info/categories/tree');

        $response->assertStatus(200);
    }

    public function test_can_view_leaf_categories()
    {
        Category::create([
            'name' => 'test category',
        ]);

        $response = $this->get('api/base-info/categories/leaf');

        $response->assertStatus(200);
    }

    public function test_can_view_root_categories()
    {
        Category::create([
            'name' => 'test category',
        ]);

        $response = $this->get('api/base-info/categories/root');

        $response->assertStatus(200);
    }
}
