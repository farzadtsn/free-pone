<?php

namespace Tests\Feature\Controllers\BaseInfo;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class BusinessControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function test_can_view_business_types()
    {
        $response = $this->get('api/base-info/businesses/types');

        $response->assertStatus(200);
    }
}
