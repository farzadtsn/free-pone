<?php

namespace Tests\Feature\Controllers\BaseInfo;

use App\Models\City;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class StateControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function test_can_view_cities_of_state()
    {
        $city = City::factory()->make();

        $response = $this->get("api/base-info/states/{$city->state_id}/cities");

        $response->assertStatus(200);
    }
}
