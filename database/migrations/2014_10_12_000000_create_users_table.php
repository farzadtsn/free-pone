<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (App::environment() !== 'testing') {
            return;
        }

        if (Schema::hasTable($this->getUserTableName()) ) {
            return;
        }

        Schema::create($this->getUserTableName(), function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable()->default(null)->index();
            $table->string('role')->default('customer')->index()->comment('values: administrator, customer');
            $table->string('email')->index()->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (App::environment() !== 'testing') {
            return;
        }

        Schema::dropIfExists($this->getUserTableName());
    }

    private function getUserTableName()
    {
        return config('database.user_schema')  . '.users';
    }
};
