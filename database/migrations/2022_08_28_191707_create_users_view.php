<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $userDbSchema = config('database.user_schema');

        $viewName = $this->getViewName();

        DB::statement("CREATE OR REPLACE VIEW {$viewName} AS (SELECT * FROM {$userDbSchema}.users)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $viewName = $this->getViewName();

        DB::statement("DROP VIEW IF EXISTS {$viewName}");
    }

    private function getViewName()
    {
        return config('database.schema') . '.users';
    }
};
