<?php

use App\Enums\DiscountCampaignStateEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_campaigns', function (Blueprint $table) {
            $table->id();
            $table->string('name', 190)->nullable();
            $table->text('description')->nullable();
            $table->string('location_enum', 30)->nullable();
            $table->string('state_enum', 30)->default(DiscountCampaignStateEnum::PENDING);
            $table->boolean('is_appointment_required')->default(false);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('deadline')->nullable()->comment('deadline days after purchasing');
            $table->smallInteger('step')->default(1)->nullable();
            $table->foreignId('business_id')->index();
            $table->foreignId('currency_id')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_campaigns');
    }
};
