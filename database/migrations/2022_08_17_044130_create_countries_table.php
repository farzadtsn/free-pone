<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->json('name');
            $table->string('en_name')->nullable();
            $table->string('iso3');
            $table->string('iso2');
            $table->string('phone_code');
            $table->string('capital');
            $table->string('currency');
            $table->string('currency_symbol');
            $table->string('tld');
            $table->string('native')->nullable();
            $table->string('region');
            $table->string('subregion');
            $table->json('timezones');
            $table->text('latitude');
            $table->text('longitude');
            $table->text('emoji');
            $table->text('emojiU');
            $table->boolean('flag')->default(false);
            $table->text('wikiDataId')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
};
