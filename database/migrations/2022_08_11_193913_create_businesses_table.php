<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100)->nullable();
            $table->string('website')->nullable();
            $table->string('address', 400)->nullable();
            $table->string('zip_code', 30)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('business_type', 100)->nullable();
            $table->string('owner_name', 100)->nullable();
            $table->text('description', 1000)->nullable();
            $table->json('opening_hours')->nullable();
            $table->foreignId('user_id')->index();
            $table->foreignId('city_id')->nullable()->index();
            $table->foreignId('state_id')->nullable()->index();
            $table->foreignId('country_id')->nullable()->index();
            $table->decimal('latitude', 10, 8)->nullable();
            $table->decimal('longitude', 11, 8)->nullable();
            $table->integer('established_year')->nullable();
            $table->smallInteger('step')->nullable()->default(1);
            $table->boolean('is_closed')->default(false);
            $table->date('reopen_date')->nullable();
            $table->text('closed_reason', 1000)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
};
