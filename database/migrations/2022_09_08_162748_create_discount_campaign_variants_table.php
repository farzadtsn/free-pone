<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_campaign_variants', function (Blueprint $table) {
            $table->id();
            $table->string('name', 256);
            $table->integer('quantity')->default(0);
            $table->integer('remained_quantity')->default(0);
            $table->decimal('price', 20, 6)->default(0);
            $table->float('discount_percent', 5, 2)->default(0);
            $table->foreignId('discount_campaign_id')->index();
            $table->foreignId('currency_id')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_campaign_variants');
    }
};
