<?php

use App\Enums\TransactionStateEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\TransactionStatusEnum;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->index();
            $table->foreignId('currency_id')->index();
            $table->foreignId('payment_gateway_id')->index();
            $table->nullableMorphs('relation');
            // withdraw, deposit
            // $table->string('type', 20)->index();
            $table->decimal('amount', 15, 8)->default(null);
            $table->string('ref_id')->index()->nullable();
            $table->string('invoice_number')->index();
            $table->json('data')->nullable()->default(null);
            $table->string('state')->default(TransactionStateEnum::PENDING);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_transactions');
    }
};
