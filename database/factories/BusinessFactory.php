<?php

namespace Database\Factories;

use App\Models\City;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Business>
 */
class BusinessFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->jobTitle,
            'website' => $this->faker->url,
            'address' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
            'step' => null,
            'established_year' => $this->faker->numberBetween(1990, 2020),
        ];
    }

    public function forCity(City $city)
    {
        return $this->state([
            'city_id' => $city->id,
            'country_id' => $city->country_id,
            'state_id' => $city->state_id,
            'latitude' => $city->latitude,
            'longitude' => $city->longitude,
        ]);
    }
}
