<?php

namespace Database\Factories;

use App\Enums\TransactionStateEnum;
use App\Models\Currency;
use App\Models\PaymentGateway;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\UserTransaction>
 */
class UserTransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $gatewayIds = PaymentGateway::pluck('id')->toArray();

        return [
            'user_id' => User::factory(),
            'currency_id' => $this->faker->randomElement(Currency::pluck('id')->toArray()),
            'payment_gateway_id' => $this->faker->randomElement($gatewayIds),
            'amount' => $this->faker->numberBetween(100, 700),
            'state' => $this->faker->randomElement(TransactionStateEnum::getValues()),
            'invoice_number' => $this->faker->numberBetween(10000, 99999),
        ];
    }
}
