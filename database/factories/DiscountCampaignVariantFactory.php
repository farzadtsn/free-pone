<?php

namespace Database\Factories;

use App\Models\DiscountCampaign;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\DiscountCampaignVariant>
 */
class DiscountCampaignVariantFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $quantity = $this->faker->numberBetween(1,15);
        return [
            'name' => $this->faker->jobTitle,
            'quantity' => $this->faker->numberBetween(0,10),
            'remained_quantity' => $this->faker->numberBetween(0,$quantity),
            'price' => $this->faker->numberBetween(50,300),
            'discount_percent' => $this->faker->numberBetween(10,60),
            'discount_campaign_id' => DiscountCampaign::factory(),
        ];
    }
}
