<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Country>
 */
class CountryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $countryCode = $this->faker->countryCode;

        return [
            'name' => [
                'en' => $this->faker->country($countryCode),
            ],
            'iso3' => $countryCode,
            'iso2' => $countryCode,
            'phone_code' => '98',
            'capital' => 'Kabul',
            'currency' => 'AFN',
            'currency_symbol' => '؋',
            'tld' => '.af',
            'native' => 'افغانستان',
            'region' => 'Asia',
            'subregion' => 'Southern Asia',
            'timezones' => '[{"zoneName":"Asia\\/Kabul","gmtOffset":16200,"gmtOffsetName":"UTC+04:30","abbreviation":"AFT","tzName":"Afghanistan Time"}]',
            'latitude' => '33.00000000',
            'longitude' => '65.00000000',
            'emoji' => '🇦🇫',
            'emojiU' => 'U+1F1E6 U+1F1EB',
            'flag' => 1,
            'wikiDataId' => 'Q'
        ];
    }
}
