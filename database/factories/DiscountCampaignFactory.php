<?php

namespace Database\Factories;

use App\Enums\DiscountCampaignLocationEnum;
use App\Enums\DiscountCampaignStateEnum;
use App\Models\Business;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\DiscountCampaign>
 */
class DiscountCampaignFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $startData = $this->faker->dateTimeBetween('-2 days', '+10 days');
        return [
            'name' => $this->faker->jobTitle,
            'description' => $this->faker->paragraph(),
            'location_enum' => $this->faker->randomElement(DiscountCampaignLocationEnum::getValues()),
            'state_enum' => $this->faker->randomElement(DiscountCampaignStateEnum::getValues()),
            'is_appointment_required' => $this->faker->boolean(),
            'start_date' => $startData->format('Y-m-d'),
            'end_date' => $this->faker->dateTimeBetween($startData, $startData->format('Y-m-d H:i:s').' +20 days')->format('Y-m-d'),
            'deadline' => $this->faker->numberBetween(1,30),
            'step' => null,
            'business_id' => Business::factory(),
        ];
    }
}
