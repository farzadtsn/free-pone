<?php

namespace Database\Factories;

use App\Models\Country;
use App\Models\State;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\City>
 */
class CityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->city,
            'state_id' => State::factory(),
            'state_code' => '07',
            'country_id' => Country::factory(),
            'country_code' => 'AD',
            'latitude' => '42.50779000',
            'longitude' => '1.52109000',
            'flag' => 1,
            'wikiDataId' => 'Q1863',
        ];
    }
}
