<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\UserBalance>
 */
class UserBalanceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $amount = $this->faker->numberBetween(-200, 200);
        return [
            'title' => $this->faker->jobTitle(),
            'type' => $amount >= 0 ? 1 : -1,
            'amount' => $amount,
            'balance' => $this->faker->numberBetween(0, 1000),
        ];
    }
}
