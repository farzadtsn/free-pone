<?php

namespace Database\Seeders;

use App\Models\PaymentGateway;
use File;
use Illuminate\Database\Seeder;

class PaymentGatewayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gateWays = [
            ['name' => 'Paypal', 'slug' => 'paypal', 'is_active' => true],
            ['name' => 'Binance', 'slug' => 'binance', 'is_active' => true],
            ['name' => 'Dogecoin', 'slug' => 'dogecoin', 'is_active' => true],
            ['name' => 'Bitcoin', 'slug' => 'bitcoin', 'is_active' => true],
        ];

        PaymentGateway::insert($gateWays);
    }
}
