<?php

namespace Database\Seeders;

use App\Models\Business;
use App\Models\Category;
use App\Models\Currency;
use App\Models\DiscountCampaign;
use App\Models\DiscountCampaignVariant;
use Illuminate\Database\Seeder;

class DiscountCampaignTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::whereIsLeaf()->get();
        $currencyId = Currency::first()->id;
        Business::with('categories')->get()->each(function($business) use ($categories, $currencyId) {
            $discountCampaign = DiscountCampaign::factory()
                ->has(DiscountCampaignVariant::factory()->count(3), 'variants')
                ->create([
                    'currency_id' => $currencyId,
                    'business_id' => $business->id
                ]);

            $discountCampaign
                ->categories()
                ->sync(
                    $business->categories->random(2)->pluck('id')->toArray()
                );
        });
    }
}
