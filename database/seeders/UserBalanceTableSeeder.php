<?php

namespace Database\Seeders;

use App\Models\Currency;
use App\Models\User;
use App\Models\UserBalance;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserBalanceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = Currency::all();
        $user = User::first();
        foreach ($currencies as $currency) {
            UserBalance::factory()
                ->count(20)
                ->create([
                    'user_id' => $user->id,
                    'currency_id' => $currency->id,
                ]);
        }
    }
}
