<?php

namespace Database\Seeders;

use App\Models\Facility;
use App\Models\FacilityGroup;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FacilityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FacilityGroup::factory(5)
            ->has(Facility::factory()->count(6))
            ->create();
    }
}
