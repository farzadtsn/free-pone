<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currenciesData = [
            [
                'name' => 'dollar',
                'symbol' => '$',
                'iso_code' => 'USD',
                'exchange_rate' => 1,
            ],
            [
                'name' => 'lira',
                'symbol' => '₺',
                'iso_code' => 'TRY',
                'exchange_rate' => 2,
            ],
        ];

        Currency::insert($currenciesData);
    }
}
