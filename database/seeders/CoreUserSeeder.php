<?php

namespace Database\Seeders;

use App\Models\Business;
use App\Models\Category;
use App\Models\City;
use App\Models\User;
use Database\Factories\BusinessFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CoreUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $usersTable = config('database.user_schema') . '.users';

        if (DB::table($usersTable)->count() < 20) {
            $users = User::factory(25)->make([
                'password' => Hash::make('password'),
            ]);
            DB::table($usersTable)->insert($users->toArray());
        }

        $cities = City::query()->inRandomOrder()->limit(20)->get();
        $categories = Category::whereIsLeaf()->get();

        User::get()->each(function($user) use ($cities, $categories) {
            $city = $cities->random(1)->first();
            $business = Business::factory()->forCity($city)->create([
                'user_id' => $user->id,
            ]);

            $business->categories()->sync($categories->random(2)->pluck('id')->toArray());
        });
    }
}
