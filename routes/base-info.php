<?php

use App\Http\Controllers\BaseInfo\BusinessController;
use App\Http\Controllers\BaseInfo\CategoryController;
use App\Http\Controllers\BaseInfo\CountryController;
use App\Http\Controllers\BaseInfo\CurrencyController;
use App\Http\Controllers\BaseInfo\FacilityController;
use App\Http\Controllers\BaseInfo\PaymentGatewayController;
use App\Http\Controllers\BaseInfo\ReportController;
use App\Http\Controllers\BaseInfo\StateController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'categories'], function () {
    Route::get('tree', [CategoryController::class, 'treeView']);
    Route::get('root', [CategoryController::class, 'root']);
    Route::get('leaf', [CategoryController::class, 'leaf']);
});

Route::group(['prefix' => 'facilities'], function () {
    Route::get('groups', [FacilityController::class, 'groups']);
});

Route::group(['prefix' => 'businesses'], function () {
    Route::get('types', [BusinessController::class, 'businessTypes']);
});

Route::group(['prefix' => 'reports'], function () {
    Route::get('photos/types', [ReportController::class, 'photoReportsType']);
});

Route::group(['prefix' => 'countries'], function () {
    Route::get('/', [CountryController::class, 'index']);
    Route::get('/{country}', [CountryController::class, 'show']);
    Route::get('/{country}/states', [CountryController::class, 'states']);
    Route::get('/{country}/cities', [CountryController::class, 'cities']);
});

Route::group(['prefix' => 'states'], function () {
    Route::get('/{state}/cities', [StateController::class, 'cities']);
});

Route::group(['prefix' => 'payment-gateways'], function () {
    Route::get('/', [PaymentGatewayController::class, 'index']);
});

Route::group(['prefix' => 'currencies'], function () {
    Route::get('/', [CurrencyController::class, 'index']);
});
