<?php

use App\Enums\UserActionTypeEnum;
use App\Http\Controllers\Auth\BusinessRegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Client\BusinessController;
use App\Http\Controllers\Client\BusinessUserActionController;
use App\Http\Controllers\Client\CommentController;
use App\Http\Controllers\Client\CommentUserActionController;
use App\Http\Controllers\Client\ProfileController;
use App\Http\Controllers\Client\ReportController;
use App\Http\Controllers\Client\SearchController;
use App\Http\Middleware\CanBeGuestMiddleware;
use App\Http\Middleware\CheckTokenMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('auth')->name('auth.')->group(function () {
    Route::post('login', [LoginController::class, 'login']);
    Route::post('register', [RegisterController::class, 'register']);
    Route::post('business/register', [BusinessRegisterController::class, 'register']);
});

Route::prefix('client')
    ->middleware([CanBeGuestMiddleware::class])
    ->group(function () {
        Route::prefix('businesses')->group(function () {
            Route::get('suggestions', [SearchController::class, 'suggestions']);
            Route::get('{business}', [BusinessController::class, 'show']);
        });

        Route::prefix('discount-campaigns')->group(function () {
            Route::get('search', [SearchController::class, 'searchDiscountCampaign']);
        });
    });

Route::prefix('client')
    ->middleware([CheckTokenMiddleware::class])
    ->group(function () {
        Route::post('/report/businesses/{business}', [ReportController::class, 'reportBusiness']);
        Route::post('/report/attachments/{attachment}', [ReportController::class, 'reportAttachment']);

        Route::post('/businesses/{business}/{action}', [BusinessUserActionController::class, 'addAction'])
            ->whereIn('action', UserActionTypeEnum::getValues());
        Route::delete('/businesses/{business}/{action}', [BusinessUserActionController::class, 'removeAction'])
            ->whereIn('action', UserActionTypeEnum::getValues());

        Route::post('/comments/{comment}/{action}', [CommentUserActionController::class, 'addAction'])
            ->whereIn('action', UserActionTypeEnum::commentActions());
        Route::delete('/comments/{comment}/{action}', [CommentUserActionController::class, 'removeAction'])
            ->whereIn('action', UserActionTypeEnum::commentActions());

        Route::prefix('businesses')
            ->group(function () {
                Route::get('/{business}/comments', [CommentController::class, 'index']);
                Route::post('/{business}/comments', [CommentController::class, 'create']);
            });

        Route::prefix('profile')->group(function () {
            Route::get('transactions', [ProfileController::class, 'transactions']);
        });
    });
