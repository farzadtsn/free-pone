<?php

use App\Http\Controllers\Admin\AttachmentController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CurrencyController;
use App\Http\Middleware\CheckTokenMiddleware;
use Illuminate\Support\Facades\Route;

Route::middleware([CheckTokenMiddleware::class])
    ->group(function () {
        Route::apiResource('categories', CategoryController::class);
        Route::apiResource('currencies', CurrencyController::class);

        Route::prefix('attachments')->controller(AttachmentController::class)->group(function () {
            Route::get('/', 'index');
            Route::get('/filters', 'filters');
            Route::post('{attachment}/approve', 'approve');
            Route::post('{attachment}/reject', 'reject');
        });
    });
