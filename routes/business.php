<?php

use App\Http\Controllers\Business\BusinessDiscountCampaignStepsController;
use App\Http\Controllers\Business\BusinessGalleryController;
use App\Http\Controllers\Business\BusinessProfileController;
use App\Http\Controllers\Business\BusinessRegisterStepsController;
use App\Http\Middleware\CheckTokenMiddleware;
use App\Http\Middleware\CheckUserIsBusiness;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware([CheckTokenMiddleware::class, CheckUserIsBusiness::class])
    ->group(function () {
        Route::post('info', [BusinessRegisterStepsController::class, 'setInfo']);
        Route::post('phone', [BusinessRegisterStepsController::class, 'setPhone']);
        Route::post('address', [BusinessRegisterStepsController::class, 'setAddress']);

        Route::prefix('discount-campaigns')->group(function () {
            Route::prefix('create')->group(function () {
                Route::post('categories', [BusinessDiscountCampaignStepsController::class, 'setCategories']);
                Route::post('title', [BusinessDiscountCampaignStepsController::class, 'setTitle']);
                Route::post('description', [BusinessDiscountCampaignStepsController::class, 'setDescription']);
                Route::post('photo', [BusinessDiscountCampaignStepsController::class, 'addPhoto']);
                Route::post('variant', [BusinessDiscountCampaignStepsController::class, 'addVariant']);
                Route::post('location', [BusinessDiscountCampaignStepsController::class, 'setLocation']);
                Route::post('appointment', [BusinessDiscountCampaignStepsController::class, 'setAppointmentType']);
                Route::post('dates', [BusinessDiscountCampaignStepsController::class, 'setDates']);
            });
        });

        Route::prefix('profile')->group(function () {
            Route::get('/', [BusinessProfileController::class, 'showProfile']);
            Route::post('basic-info', [BusinessProfileController::class, 'updateBasicInfo']);
            Route::post('bio', [BusinessProfileController::class, 'updateBio']);
            Route::post('address', [BusinessProfileController::class, 'updateAddress']);
            Route::post('photo', [BusinessGalleryController::class, 'addToGallery']);
            Route::put('photo/{attachment}', [BusinessGalleryController::class, 'updatePhotoData']);
            Route::post('opening-hours', [BusinessProfileController::class, 'updateOpeningHours']);
            Route::post('facilities', [BusinessProfileController::class, 'updateFacilities']);
            Route::post('categories', [BusinessProfileController::class, 'updateCategories']);
            Route::post('close-business', [BusinessProfileController::class, 'closeBusiness']);
            Route::delete('photo/{attachment}', [BusinessGalleryController::class, 'removeFromGallery']);
        });
    });
